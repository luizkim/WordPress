<?php

namespace Sales\Model;

use Core\Model\DefaultModel;
use Core\Model\ErrorModel;
use Company\Model\CompanyModel;
use Itaucripto\Itaucripto;
use Core\Helper\Api;
use Core\Helper\Nfe;
use Sales\Model\ShippingModel;
use Core\Helper\Mautic;
use Core\Helper\Mail;
use Core\Helper\Config;
use Core\Helper\ReadMail;
use Core\Helper\Format;

class OrderModel extends DefaultModel {

    /**
     * @var \Company\Model\CompanyModel $entity   
     */
    protected $_companyModel;

    public function initialize(\Zend\ServiceManager\ServiceManager $serviceLocator) {
        parent::initialize($serviceLocator);
        $this->_companyModel = new CompanyModel();
        $this->_companyModel->initialize($serviceLocator);
    }

	public function approveOrderFromInvoiceTax(){		
				$shippingModel = new ShippingModel();
				$shippingModel->initialize($this->serviceLocator);	
				$result = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('O')
                        ->select()
                        ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'O.id = OIT.order')
                        ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'IT.id = OIT.invoice_tax')                        
                        ->andWhere('OIT.invoice_type =:invoice_type')                        
						->andWhere('O.order_status =:order_status')
						->setMaxResults(1)
                        ->setParameters(array(                            
                            'invoice_type' => 55,                            
							'order_status'=> $shippingModel->discoveryOrderStatus('automatic analysis', 'closed', 1, 0)
                        ))						
						->getQuery()->getResult();		
				foreach ($result AS $order){					
						$nf = simplexml_load_string($order->getInvoiceTax()[0]->getInvoiceTax()->getInvoice());
						if ($nf){
							$transporte = $nf->NFe->infNFe->transp;
							if ($transporte->modFrete != 2){
								ErrorModel::addError('Freight payer field must be 2');
							}
							$documents = $order->getProvider()->getDocument();
							foreach ($documents AS $document){
								if ($document->getDocumentType()->getDocumentType() == 'CNPJ'){																		
									$cnpj = Format::onlyNumbers($document->getDocument());
								}
							}							
							if (!$cnpj || !preg_match('/'.$cnpj.'/', Format::onlyNumbers((string)$nf->NFe->infNFe->infAdic->infCpl))){
								ErrorModel::addError('Additional information is required');
							}
							$valor_frete = number_format((string)$nf->NFe->infNFe->det->prod->vFrete, 2, '.', '');							
							if ($valor_frete > 0 && $valor_frete != number_format($order->getPrice(), 2, '.', '')){
								ErrorModel::addError('Freight value entered on invoice is incorrect');
							}							
							if ($nf->NFe->infNFe->total->ICMSTot->vProd != $order->getInvoiceTotal()){
								ErrorModel::addError('Invoice tax value entered is incorrect');
							}
							if ($nf->NFe->infNFe->transp->vol->pesoB > $order->getCubage()){
								ErrorModel::addError('The weight informed on the invoice is greater than the quotation');
							}
							
							$emitente = $nf->NFe->infNFe->emit;
							if (Format::onlyNumbers($emitente->enderEmit->CEP) != $order->getAddressOrigin()->getStreet()->getCep()->getCep()){
								ErrorModel::addError('CEP origin entered is divergent of quote');
							}
							if (Format::cleanString($emitente->enderEmit->xLgr) != Format::cleanString($order->getAddressOrigin()->getStreet()->getStreet())){
								ErrorModel::addError('Street of address origin entered is divergent of quote');
							}	
							if (Format::cleanString($emitente->enderEmit->nro) != Format::cleanString($order->getAddressOrigin()->getNumber())){
								ErrorModel::addError('Number of address origin entered is divergent of quote');
							}		
							if (Format::cleanString($emitente->enderEmit->xBairro) != Format::cleanString($order->getAddressOrigin()->getStreet()->getDistrict()->getDistrict())){
								ErrorModel::addError('Number of address origin entered is divergent of quote');
							}		
							if (Format::cleanString($emitente->enderEmit->xMun) != Format::cleanString($order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getCity())){
								ErrorModel::addError('City origin entered is divergent of quote');
							}							
							if (Format::cleanString($emitente->enderEmit->UF) != Format::cleanString($order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf())){
								ErrorModel::addError('UF origin entered is divergent of quote');
							}																						
							$destinatario = $nf->NFe->infNFe->dest;
							if (Format::onlyNumbers($destinatario->enderDest->CEP) != $order->getAddressDestination()->getStreet()->getCep()->getCep()){
								ErrorModel::addError('CEP destination entered is divergent of quote');
							}
							if (Format::cleanString($destinatario->enderDest->xLgr) != Format::cleanString($order->getAddressDestination()->getStreet()->getStreet())){
								ErrorModel::addError('Street of address destination entered is divergent of quote');
							}	
							if (Format::cleanString($destinatario->enderDest->nro) != Format::cleanString($order->getAddressDestination()->getNumber())){
								ErrorModel::addError('Number of address destination entered is divergent of quote');
							}		
							if (Format::cleanString($destinatario->enderDest->xBairro) != Format::cleanString($order->getAddressDestination()->getStreet()->getDistrict()->getDistrict())){
								ErrorModel::addError('Number of address destination entered is divergent of quote');
							}		
							if (Format::cleanString($destinatario->enderDest->xMun) != Format::cleanString($order->getAddressDestination()->getStreet()->getDistrict()->getCity()->getCity())){
								ErrorModel::addError('City destination entered is divergent of quote');
							}							
							if (Format::cleanString($destinatario->enderDest->UF) != Format::cleanString($order->getAddressDestination()->getStreet()->getDistrict()->getCity()->getState()->getUf())){
								ErrorModel::addError('UF destination entered is divergent of quote');
							}																						
						}else{
							ErrorModel::addError('Error reading invoice XML');
						}				
						
						$transportadora = $nf->NFe->infNFe->transp->transporta;												
						$documents = $order->getQuote()->getCarrier()->getDocument();
						foreach ($documents AS $document){
							if ($document->getDocumentType()->getDocumentType() == 'CNPJ'){																		
								$cnpj_transportadora = Format::onlyNumbers($document->getDocument());
							}
						}
						if (!$cnpj_transportadora || !preg_match('/'.$cnpj_transportadora.'/', Format::onlyNumbers((string)$transportadora->CNPJ))){
							ErrorModel::addError('Document of carrier is incorrect');
						}
						
						
						if (ErrorModel::getErrors()){
							foreach (ErrorModel::getErrors() AS $error){
								$msg .= $error['message'].PHP_EOL;
							}	
							$order->setComments($order->getComments().PHP_EOL.$msg);
							$order->setStatus($shippingModel->discoveryOrderStatus('analysis', 'pending', 1, 0));							
							$this->persist($order);
						}else{
							$this->approveOrder($order->getId());							
						}								
				}				
	}

	
	public function uploadCTEFromMail(){		
		$attachments = ReadMail::getAttachments();										
		foreach ($attachments AS $attachment){						
			switch (pathinfo($attachment['filename'], PATHINFO_EXTENSION)) {
				case 'zip':
					$attachment = $this->getDacteFromZip($attachment);
					$this->processXml($attachment);
					break;
				case 'xml':
					$this->processXml($attachment);
					break;
			}		    			
			ReadMail::remove($attachment['mail']);
		}
		ReadMail::close();
	}
	
	protected function getDacteFromZip($attachment){
		$temp_file = tempnam(sys_get_temp_dir(), 'zip');		
		file_put_contents( $temp_file, $attachment['attachment']);
		$zip = new \ZipArchive;
		$res = $zip->open($temp_file);
		if ($res === TRUE) {						
			for( $i = 0; $i < $zip->numFiles; $i++ ){ 
				$stat = $zip->statIndex($i);
				if (pathinfo($stat['name'], PATHINFO_EXTENSION) === 'xml'){				
					$attachment['attachment'] = $zip->getFromIndex($i);				
				}
			}		  
		  $zip->close();    
		} 		
		unlink($temp_file);		
		return $attachment;
	}
	
	protected function processXml($attachment){
			$nf = simplexml_load_string($attachment['attachment']);
			if ($nf){
				foreach ($nf->CTe->infCte->infCTeNorm->infDoc->infNFe AS $nf_key) {				
					$nf_number = (int) substr((string) $nf_key->chave, 25, 9);
					$nfs[] = $nf_number;				
				}	
				$shippingModel = new ShippingModel();
				$shippingModel->initialize($this->serviceLocator);						
				$result = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('O')
                        ->select()
                        ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'O.id = OIT.order')
                        ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'IT.id = OIT.invoice_tax')                        
                        ->andWhere('OIT.invoice_type =:invoice_type')
                        ->andWhere('IT.invoice_number IN (:invoice)')
						->andWhere('O.order_status =:order_status')
                        ->setParameters(array(                            
                            'invoice_type' => 55,
                            'invoice' => $nfs,
							'order_status'=> $shippingModel->discoveryOrderStatus('waiting retrieve', 'open', 1, 1)
                        ))->getQuery()->getResult();						
				if ($result){
					$this->addCarrierInvoiceTax($result[0],$attachment['attachment']);					
				}   
			}			
	}
	
	public function chooseFirstQuote(){				
	
		$sql = 'UPDATE orders
				INNER JOIN quote ON (quote.order_id = orders.id)				
				SET orders.quote_id = quote.id                
				WHERE orders.quote_id IS NULL';				
		$stmt = $this->_em->getConnection()->prepare($sql);        
        $stmt->execute();
		
	}
    public function getClientInvoiceTaxFromOrder(\Core\Entity\Order $order) {
        $result = $this->_em->getRepository('\Core\Entity\InvoiceTax')
                        ->createQueryBuilder('IT')
                        ->select()
                        ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'IT.id = OIT.invoice_tax')
                        ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OIT.order')
                        ->where('OIT.issuer =:issuer')
                        ->andWhere('OIT.invoice_type =:invoice_type')
                        ->andWhere('O.id =:order')
                        ->setParameters(array(
                            'issuer' => $order->getClient(),
                            'invoice_type' => 55,
                            'order' => $order
                        ))->getQuery()->getResult();
        return $result ? $result[0] : NULL;
    }

    public function getCarrierInvoiceTaxFromOrder(\Core\Entity\Order $order) {
        if ($order->getQuote()) {
            $result = $this->_em->getRepository('\Core\Entity\InvoiceTax')
                            ->createQueryBuilder('IT')
                            ->select()
                            ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'IT.id = OIT.invoice_tax')
                            ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OIT.order')
                            ->where('OIT.issuer =:issuer')
                            ->andWhere('OIT.invoice_type =:invoice_type')
                            ->andWhere('O.id =:order')
                            ->setParameters(array(
                                'issuer' => $order->getQuote()->getCarrier(),
                                'invoice_type' => 57,
                                'order' => $order
                            ))->getQuery()->getResult();
        }
        return $result ? $result[0] : NULL;
    }

    public function clearInvoices() {
        /*        
        echo $this->_em->createQueryBuilder()->delete('\Core\Entity\Invoice', 'I')
                ->where('I.id IN (SELECT id FROM `invoice` WHERE id NOT IN (SELECT invoice_id FROM order_invoice))')
                ->getQuery()->getSQL()->execute();                         
         */
    }

	
	public function changeOwnerOrders(){
		/*
		Not work updates with join in doctrine
		return $this->_em->createQueryBuilder()->update('\Core\Entity\Quote', 'q')
		        ->innerJoin('\Core\Entity\City', 'C', 'WITH', 'q.city_origin_id = c.id')
				->innerJoin('\Core\Entity\PeopleStates', 'ps', 'WITH', 'ps.state_id = c.state_id')
				->set('q.provider_id', 'ps.people_id')
				->where('q.provider_id IS NOT NULL;')				
				->getQuery()->execute();
		*/
		
		
		
		$sql = 'UPDATE quote
				INNER JOIN city ON (quote.city_origin_id = city.id)
				INNER JOIN people_states ON (people_states.state_id = city.state_id)
				SET quote.provider_id = people_states.people_id
				WHERE quote.provider_id IS NOT NULL';				
		$stmt = $this->_em->getConnection()->prepare($sql);        
        $stmt->execute();
		

		$sql = 'UPDATE orders    
				INNER JOIN quote ON (quote.id = orders.quote_id)
				INNER JOIN city ON (quote.city_origin_id = city.id)
				INNER JOIN people_states ON (people_states.state_id = city.state_id)
				SET orders.provider_id = people_states.people_id
				WHERE orders.quote_id IS NOT NULL
				AND orders.provider_id IN (SELECT people_states.people_id FROM people_states)
				AND address_origin_id IS NOT NULL
				AND address_destination_id IS NOT NULL';				
		$stmt = $this->_em->getConnection()->prepare($sql);        
        $stmt->execute();


		$sql = 'UPDATE orders    
				INNER JOIN quote ON (quote.id = orders.quote_id)
				INNER JOIN city ON (quote.city_origin_id = city.id)
				INNER JOIN people_states ON (people_states.state_id = city.state_id)
				SET orders.client_id = people_states.people_id
				WHERE orders.quote_id IS NOT NULL
				AND orders.client_id IN (SELECT people_states.people_id FROM people_states)
				AND address_origin_id IS NOT NULL
				AND address_destination_id IS NOT NULL';				
		$stmt = $this->_em->getConnection()->prepare($sql);        
        $stmt->execute();
		
		/*
		$sql = 'UPDATE orders    				
				SET orders.payer_people_id = orders.provider_id
				WHERE orders.quote_id IS NOT NULL
				AND orders.payer_people_id IN (SELECT people_states.people_id FROM people_states)
				AND address_origin_id IS NOT NULL
				AND address_destination_id IS NOT NULL';				
		$stmt = $this->_em->getConnection()->prepare($sql);        
        $stmt->execute();		
		*/
				
	}
	
    public function generateInvoiceTax() {
        /*
         * @todo Gerar nota fiscal automaticamente e depois mudar para o status abaixo:
         */
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        return $this->_em->createQueryBuilder()->update('\Core\Entity\Invoice', 'i')
                        ->set('i.invoice_status', $shippingModel->discoveryInvoiceStatus('waiting billing', 'open', 1, 0)->getId())
                        ->where('i.invoice_status IN (:status)')
                        ->setParameter('status', array(
                            $shippingModel->discoveryInvoiceStatus('waiting generate invoice', 'open', 1, 0)
                        ))
                        ->getQuery()->execute();
    }

    public function closeBillingInvoice() {
        $next = strtotime('last Saturday');
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $date = new \DateTime();
        $date->setDate(date('Y', $next), date('m', $next), date('d', $next));
        return $this->_em->createQueryBuilder()->update('\Core\Entity\Invoice', 'i')
                        ->set('i.invoice_status', $shippingModel->discoveryInvoiceStatus('waiting generate invoice', 'open', 1, 0)->getId())
                        ->where('i.invoice_status IN (:status)')
                        ->andWhere('i.invoice_date < :invoice_date')
                        ->setParameter('invoice_date', $date)
                        ->setParameter('status', array(
                            $shippingModel->discoveryInvoiceStatus('open', 'pending', 1, 0)
                        ))
                        ->getQuery()->execute();
    }

    public function generatePurchasingInvoice() {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $result = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('O')
                        ->select('O')
                        ->addSelect('OI')
                        ->leftJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.order = O.id')
                        ->andWhere('O.client =:client')
                        ->andWhere('O.order_status NOT IN(:order_status)')
                        ->andWhere('OI.id IS NULL')
                        ->andWhere('O.provider IS NOT NULL')
                        ->setParameters(array(
                            'order_status' => array(
                                $shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
                                $shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)
                            ),
                            'client' => $this->_companyModel->getLoggedPeopleCompany()
                        ))
                        ->setMaxResults(1)
                        ->getQuery()->getResult();

        $order = $result ? $result[0] : NULL;
        if (count($order) > 0) {
            $provider = $order->getProvider();
            if (count($provider) > 0) {
                $this->createPurchasingInvoice($order, $provider);
            }
        }
    }

    public function generateBillingInvoice() {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);

        $invoice = $this->_em->getRepository('\Core\Entity\Invoice')->findOneBy(array(
            'invoice_status' => $shippingModel->discoveryInvoiceStatus('waiting billing', 'open', 1, 0)->getId()
        ));
        if ($invoice && $invoice->getOrder()) {
            $financeModel = new \Finance\Model\FinanceModel();
            $financeModel->initialize($this->serviceLocator);
            $invoice = $financeModel->recalculateReceiveInvoiceAmount($invoice->getId());
            if ($invoice->getOrder()[0]->getOrder()->getProvider()->getId() == $this->_companyModel->getDefaultCompany()->getId()) {
                $order = $invoice->getOrder()[0]->getOrder();
                $orderModel = new OrderModel();
				
				
				$companyModel = new CompanyModel();
				$companyModel->initialize($this->serviceLocator);		
				$state = $order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf();
				$formID = Mautic::getFormID('invoice-payment',strtolower($state));
				$params['mauticform[formId]'] = $formID;				
				
				
                $params['mauticform[f_key]'] = Mautic::getOAuth2PublicKey();
                $params['mauticform[body]'] = Mail::renderMail($this->serviceLocator, 'client-invoice.phtml', array(
                            'saleOrders' => $financeModel->getSalesOrdersFromSaleInvoice($invoice->getId()),
                            'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                            'order' => $order, 'itauData' => $orderModel->getItauDataFromOrder($order)
                ));
                if ($order->getPayer()->getPeopleType() == 'J') {
                    $params['mauticform[email]'] = $order->getPayer()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
                } else {
                    $params['mauticform[email]'] = $order->getPayer()->getEmail()[0]->getEmail();
                }
                $client = new \Core\Helper\Api();
                $client->post(Mautic::getMauticFormAction($formID), array('form_params' => $params));
            }
            $invoice->setStatus($shippingModel->discoveryInvoiceStatus('waiting payment', 'open', 1, 1));
            $this->persist($invoice);
            //$this->_em->flush($invoice);
        }
    }

    public function cancelSaleOrder($order_id) {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        return $this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
                        ->set('o.order_status', $shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)->getId())
                        ->where('o.order_status IN (:status)')
                        ->andWhere('o.id = :order')
                        ->andWhere('o.provider = :provider')
                        ->setParameter('provider', $this->_companyModel->getLoggedPeopleCompany())
                        ->setParameter('order', $order_id)
                        ->setParameter('status', array(
                            $shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
                            $shippingModel->discoveryOrderStatus('waiting client invoice tax', 'pending', 1, 1),
                            $shippingModel->discoveryOrderStatus('automatic analysis', 'closed', 1, 0),
                            $shippingModel->discoveryOrderStatus('waiting payment', 'pending', 1, 1)
                        ))
                        ->getQuery()->execute();
    }

    public function selectFirstQuote() {
        $result = $this->_em->getRepository('\Core\Entity\Quote')
                        ->createQueryBuilder('Q')
                        ->select('Q')
                        ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = Q.order')
                        ->andWhere('O.quote IS NULL')
                        ->setMaxResults(1)
                        ->getQuery()->getResult();

        $quote = $result ? $result[0] : NULL;
        if ($quote) {
            $order = $this->_em->getRepository('\Core\Entity\Order')->find($quote->getOrder()->getId());
            if ($order) {
                $order->setQuote($quote);
                $order->setPrice($quote->getTotal());
                $this->persist($order);
                //$this->_em->flush($order);
            }
        }
    }

    public function clearOrderInvoice() {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $this->_em->createQueryBuilder()->update('\Core\Entity\Invoice', 'i')
                ->set('i.invoice_status', $shippingModel->discoveryInvoiceStatus('outdated billing', 'pending', 1, 1)->getId())
                ->where('i.invoice_status IN (:status)')
                ->andWhere('CURRENT_DATE() > DATE_ADD(i.due_date,5, \'DAY\')')                
                ->setParameter('status', array(
                    $shippingModel->discoveryInvoiceStatus('waiting payment', 'pending', 1, 1)
                ))
                ->getQuery()->execute();
    }
	
 public function createMainCommissionOrderFromSaleOrder(\Core\Entity\Order $order) {
 
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $purchasingOrder = clone $order;
        $this->_em->detach($purchasingOrder);
        $purchasingOrder->resetId();
        $purchasingOrder->setClient($order->getQuote()->getProvider());
        $purchasingOrder->setPayer($order->getQuote()->getProvider());
        $purchasingOrder->setProvider($this->_companyModel->getMainDefaultCompany());
		$purchasingOrder->setAddressOrigin(NULL);
		$purchasingOrder->setAddressDestination(NULL);
        $purchasingOrder->setPrice($order->getPrice()/100*5);
		$purchasingOrder->setStatus($shippingModel->discoveryOrderStatus('delivered', 'closed', 1, 1));
		$this->persist($purchasingOrder);
		//$this->_em->flush($purchasingOrder);		
  
        return $this->createCommissionPurchasingInvoice($purchasingOrder);
        
    }	
	
	public function createCommissionPurchasingInvoice(\Core\Entity\Order $order) {

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);

		$date = new \DateTime();
		
		$invoice = new \Core\Entity\Invoice();
		$invoice->setPrice($order->getPrice());
		$invoice->setDueDate($date);
		$invoice->setStatus($shippingModel->discoveryInvoiceStatus('waiting invoice tax', closed, 1, 1));
		$invoice->setPaymentDate(NULL);

        $this->persist($invoice);
        //$this->_em->flush($invoice);

        $orderInvoice = new \Core\Entity\OrderInvoice();
        $orderInvoice->setInvoice($invoice);
        $orderInvoice->setOrder($order);
        $this->persist($orderInvoice);
        //$this->_em->flush($orderInvoice);	

        return $invoice;
    }
	
	public function createMainCommission(){
		
		$shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);        		
        $orders = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('o')
                        ->select('o')                        						
                        ->where('o.order_status IN (:status)')	
						->andWhere('o.provider = :provider')						
                        ->setParameter('status', array(
                            $shippingModel->discoveryOrderStatus('waiting commission', 'pending', 1, 1)
                        ))
						->setParameter('provider', $this->_companyModel->getLoggedPeopleCompany())						
						->setMaxResults(1)
						->setFirstResult(0)
                        ->getQuery()->getResult();		
						
						
        if ($orders && $orders[0]) {								
			if ($orders[0]->getProvider()->getId() != $this->_companyModel->getMainDefaultCompany()->getId()){																    
				$this->createMainCommissionOrderFromSaleOrder($orders[0]);								
			}			
			
			$this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
					->set('o.order_status', $shippingModel->discoveryOrderStatus('waiting invoice tax', closed, 1, 1)->getId())
					->where('o.id IN (:id)')
					->setParameter('id', $orders[0]->getId())						
					->getQuery()->execute();									
			
        }
		
	}	
	
	public function prepareOrderInvoices(){	
		$shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $date = new \DateTime();
        $orders = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('o')
                        ->select('o.id')
                        ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.id = o.quote')
                        ->where('o.order_status IN (:status)')
                        ->andWhere('(o.provider = :provider OR o.client = :client)')
                        ->andWhere('o.alter_date < DATE_SUB(CURRENT_DATE(),(Q.deadline +1), \'day\')')
                        ->setParameter('provider', $this->_companyModel->getLoggedPeopleCompany())
                        ->setParameter('client', $this->_companyModel->getLoggedPeopleCompany())
                        ->setParameter('status', array(
                            $shippingModel->discoveryOrderStatus('on the way', 'closed', 1, 1)
                        ))
                        ->getQuery()->getResult();
        if ($orders) {
            $this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
                    ->set('o.order_status', $shippingModel->discoveryOrderStatus('waiting invoice tax', closed, 1, 1)->getId())
                    ->where('o.id IN (:id)')
                    ->setParameter('id', $orders)->getQuery()->execute();
        }
	}
		
	public function generateOrderInvoice(){	
		$shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
		$invoice_order = $this->_em->getRepository('\Core\Entity\Order')
                        ->createQueryBuilder('o')
                        ->select('o.id')
                        ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'o.id = OI.order')
						->innerJoin('\Core\Entity\Invoice', 'I', 'WITH', 'I.id = OI.invoice')
                        ->where('o.order_status IN (:status)')
                        ->andWhere('(o.provider = :provider OR o.client = :client)')                        
						->andWhere('I.invoice_status IN (:invoice_status)')                        
                        ->setParameter('provider', $this->_companyModel->getLoggedPeopleCompany())
                        ->setParameter('client', $this->_companyModel->getLoggedPeopleCompany())						
						->setParameter('invoice_status', array(
                            $shippingModel->discoveryInvoiceStatus('paid', 'closed', 1, 1)
                        ))
                        ->setParameter('status', array(
                            $shippingModel->discoveryOrderStatus('waiting invoice tax', closed, 1, 1)
                        ))
						->setMaxResults(1)
						->setFirstResult(0)
                        ->getQuery()->getResult();
						
		if ($invoice_order){						
			$order = $this->getSaleOrder($invoice_order[0]['id']);
			$nfe = new Nfe();
			$nfe->makeNfseFromOrder($order);			
		}
		
	}
    public function clearSaleOrders() {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
                ->set('o.order_status', $shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)->getId())
                ->where('o.order_status IN (:status)')
                ->andWhere('o.provider = :provider')
                ->andWhere('o.alter_date < DATE_SUB(CURRENT_DATE(),15, \'day\')')
                ->setParameter('provider', $this->_companyModel->getLoggedPeopleCompany())
                ->setParameter('status', array(
                    $shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
                    $shippingModel->discoveryOrderStatus('waiting commission', 'closed', 1, 1)
                ))
                ->getQuery()->execute();
    }

    public function discoveryInvoiceNumber(\Core\Entity\InvoiceTax $invoice) {
        if (!$invoice->getInvoiceNumber()) {
            $nf = simplexml_load_string($invoice->getInvoice());            
            $invoice->setInvoiceNumber($nf->infNFe->ide->nNF?:$nf->NFe->infNFe->ide->nNF ? : $nf->CTe->infCte->ide->nCT);
            $this->persist($invoice);
            //$this->_em->flush($invoice);
        }        
        return $invoice->getInvoiceNumber();
    }

    public function addClientInvoiceTax(\Core\Entity\Order $order, $invoice) {
        if ($order->getStatus()->getStatus() == 'waiting client invoice tax' || $order->getStatus()->getStatus() == 'analysis') {
            $clientInvoiceTax = $this->getClientInvoiceTaxFromOrder($order);
            if (!$clientInvoiceTax) {
                $clientInvoiceTax = new \Core\Entity\InvoiceTax();
                $clientInvoiceTax->setInvoice($invoice);
                $clientInvoiceTax->setInvoiceNumber($this->discoveryInvoiceNumber($clientInvoiceTax));
                $this->persist($clientInvoiceTax);
                //$this->_em->flush($clientInvoiceTax);

                $orderInvoiceTax = new \Core\Entity\OrderInvoiceTax();
                $orderInvoiceTax->setInvoiceTax($clientInvoiceTax);
                $orderInvoiceTax->setInvoiceType(55);
                $orderInvoiceTax->setIssuer($order->getClient());
                $orderInvoiceTax->setOrder($order);
                $this->persist($orderInvoiceTax);
                //$this->_em->flush($orderInvoiceTax);
            } else {
                $clientInvoiceTax->setInvoice($invoice);
                $clientInvoiceTax->setInvoiceNumber(NULL);
                $this->persist($clientInvoiceTax);
                //$this->_em->flush($clientInvoiceTax);
            }
            $shippingModel = new ShippingModel();
            $shippingModel->initialize($this->serviceLocator);
            $orderStatus = $shippingModel->discoveryOrderStatus('automatic analysis', 'closed', 1, 0);
            $order->setStatus($orderStatus);
            $this->persist($order);
            //$this->_em->flush($order);
        } else {
            ErrorModel::addError('Order is not in the correct status for sending invoice tax');
        }
        return $clientInvoiceTax;
    }

    public function verifyPayment(\Core\Entity\Order $order) {
        $params = array('DC' => $this->getItauStatusDataFromOrder($order, 1));
        $client = new \Core\Helper\Api();
        $res = $client->post(Config::getConfig('itau-shopline-status-url'), array('form_params' => $params));
        $result = $res->getBody();
        $xml = simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($xml && $xml->PARAMETER && $xml->PARAMETER->PARAM) {
            foreach ($xml->PARAMETER->PARAM AS $param) {
                $return[(string) $param->attributes()->ID] = (string) $param->attributes()->VALUE;
            }
        }
		if ($return && $return['sitPag'] == '01') {
			$invoice = $order->getInvoice() ? $order->getInvoice()[0]->getInvoice() : NULL;
			if ($invoice && $invoice->getStatus()->getStatus() == 'outdated billing') {
				$date = new \DateTime();
				$date->modify('+1 day');
				$invoice->setDueDate($date);
				$this->persist($invoice);
			}			
		}elseif ($return && $return['sitPag'] == '00') {
            $invoice = $order->getInvoice() ? $order->getInvoice()[0]->getInvoice() : NULL;
            $shippingModel = new ShippingModel();
            $shippingModel->initialize($this->serviceLocator);
//            if (($invoice && $invoice->getPrice() != $order->getPrice()) || $return['Valor'] != number_format($order->getPrice(), 2, ',', '.')) {
//                $invoiceStatus = $shippingModel->discoveryInvoiceStatus('divergence of values', 'open', 1, 0);
//                $invoice->setStatus($invoiceStatus);
//                $invoice->setInvoiceType('ITAÚ SHOPLINE');
//                /*
//                 * @todo Ajustar a data correta de pagamento $return['DtPag'] Formato: ddmmaaaa
//                 */
//                $invoice->setInvoiceSubtype($return['tipCart']);
//                $invoice->setPaymentResponse($xml);
//                $this->persist($invoice);
//                //$this->_em->flush($invoice);
//            } else 

            if ($invoice && $order->getStatus()->getStatus() == 'waiting payment') {
                $orderStatus = $shippingModel->discoveryOrderStatus('waiting retrieve', 'open', 1, 1);
                $invoiceStatus = $shippingModel->discoveryInvoiceStatus('paid', 'closed', 1, 1);
                $invoice->setStatus($invoiceStatus);
                $invoice->setInvoiceType('ITAÚ SHOPLINE');
                /*
                 * @todo Ajustar a data correta de pagamento $return['DtPag'] Formato: ddmmaaaa
                 */
                $invoice->setPaymentDate(new \DateTime());
                $invoice->setInvoiceSubtype($return['tipCart']);
                $invoice->setPaymentResponse($xml);
                $order->setStatus($orderStatus);

                $this->persist($invoice);                
                $this->persist($order);                
				$companyModel = new CompanyModel();
				$companyModel->initialize($this->serviceLocator);		
				$state = $order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf();				
				$formID = Mautic::getFormID('retrieve-request',strtolower($state));
				$params['mauticform[formId]'] = $formID;					                
                $params['mauticform[f_key]'] = Mautic::getOAuth2PublicKey();
                $params['mauticform[body]'] = Mail::renderMail($this->serviceLocator, 'retrieve-request.phtml', array(
                            'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                            'order' => $order
                ));
                $params['mauticform[email]'] = $order->getQuote()->getCarrier()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();

                $client = new \Core\Helper\Api();
                $client->post(Mautic::getMauticFormAction($formID), array('form_params' => $params));
            } else if ($invoice && in_array($invoice->getStatus()->getStatus(), array('waiting payment', 'outdated billing', 'open'))) {
                $invoiceStatus = $shippingModel->discoveryInvoiceStatus('paid', 'closed', 1, 1);
                $invoice->setStatus($invoiceStatus);
                $invoice->setInvoiceType('ITAÚ SHOPLINE');
                /*
                 * @todo Ajustar a data correta de pagamento $return['DtPag'] Formato: ddmmaaaa
                 */
                $invoice->setPaymentDate(new \DateTime());
                $invoice->setInvoiceSubtype($return['tipCart']);
                $invoice->setPaymentResponse($xml);

                $this->persist($invoice);
                //$this->_em->flush($invoice);
            }
        }
        return $return;
    }

    public function getItauStatusDataFromOrder(\Core\Entity\Order $order, $returnType = 0) {
        $invoice = $order->getInvoice() ? $order->getInvoice()[0]->getInvoice() : NULL;
        if ($invoice) {
            $codEmp = Config::getConfig('itau-shopline-company'); //Coloque o cÃ³digo da empresa em MAIÚSCULO
            $chave = Config::getConfig('itau-shopline-key'); //Coloque a chave de criptografia em MAIÚSCULO
            $cripto = new Itaucripto();
            return $cripto->geraConsulta($codEmp, $invoice->getId(), $returnType, $chave);
        }
    }

    public function getItauDataFromOrder(\Core\Entity\Order $order) {
        $invoice = $order->getInvoice() ? $order->getInvoice()[0]->getInvoice() : NULL;
        if ($invoice) {

            $codEmp = Config::getConfig('itau-shopline-company'); //Coloque o cÃ³digo da empresa em MAIÚSCULO
            $chave = Config::getConfig('itau-shopline-key'); //Coloque a chave de criptografia em MAIÚSCULO

            $pedido = $invoice->getId(); // IdentificaÃ§Ã£o do pedido - mÃ¡ximo de 8 dÃ­gitos (12345678) - ObrigatÃ³rio  
            $valor = number_format($invoice->getPrice(), 2, ',', ''); // Valor do pedido - mÃ¡ximo de 8 dÃ­gitos + vÃ­rgula + 2 dÃ­gitos - 99999999,99 - ObrigatÃ³rio                      
            $nomeSacado = $order->getPayer()->getName(); // Nome do sacado - mÃ¡ximo de 30 caracteres          
            $codigoInscricao = $order->getPayer()->getPeopleType() == 'J' ? '02' : '01'; // CÃ³digo de InscriÃ§Ã£o: 01->CPF, 02->CNPJ  

            foreach ($order->getPayer()->getDocument() AS $document) {
                if ($document->getDocumentType()->getDocumentType() == ($order->getPayer()->getPeopleType() == 'J' ? 'CNPJ' : 'CPF')) {
                    $numeroInscricao = $document->getDocument(); // NÃºmero de InscriÃ§Ã£o: CPF ou CNPJ - atÃ© 14 caracteres  
                }
            }

            if ($order->getPayer()->getId() == $order->getRetrievePeople()->getId()) {
                $address = $order->getAddressOrigin();
            } else {
                $address = $order->getAddressDestination();
            }

            $enderecoSacado = $address->getStreet()->getStreet() . ', ' . $address->getNumber() . $address->getComplement() ? ' - ' . $address->getComplement() : ''; // Endereco do Sacado - mÃ¡ximo de 40 caracteres  
            $cepSacado = str_pad($address->getStreet()->getCep()->getCep(), 8, '0', STR_PAD_LEFT); // Cep do Sacado - mÃ¡ximo de 8 dÃ­gitos  
            $bairroSacado = $address->getStreet()->getDistrict()->getDistrict(); // Bairro do Sacado - mÃ¡ximo de 15 caracteres  
            $cidadeSacado = $address->getStreet()->getDistrict()->getCity()->getCity(); // Cidade do sacado - mÃ¡ximo 15 caracteres  
            $estadoSacado = $address->getStreet()->getDistrict()->getCity()->getState()->getUf(); // Estado do Sacado - 2 caracteres              

            $dataVencimento = $order->getInvoice()[0]->getInvoice()->getDueDate()->format('dmY'); // Vencimento do tÃ­tulo - 8 dÃ­gitos - ddmmaaaa
            $urlRetorna = 'freteclick.com.br/purchasing/order/id/' . $pedido; // URL do retorno - mÃ¡ximo de 60 caracteres  
            $observacao = ""; // ObservaÃ§Ãµes - mÃ¡ximo de 40 caracteres  
            $obsAd1 = ''; // ObsAdicional1 - mÃ¡ximo de 60 caracteres  
            $obsAd2 = ''; // ObsAdicional2 - mÃ¡ximo de 60 caracteres  
            $obsAd3 = ''; // ObsAdicional3 - mÃ¡ximo de 60 caracteres        
            $cripto = new Itaucripto();
            return $cripto->geraDados($codEmp, $pedido, $valor, $observacao, $chave, $nomeSacado, $codigoInscricao, $numeroInscricao, $enderecoSacado, $bairroSacado, $cepSacado, $cidadeSacado, $estadoSacado, $dataVencimento, $urlRetorna, $obsAd1, $obsAd2, $obsAd3);
        }
    }

    /**
     * @return \Core\Entity\Order
     */
    public function getEmailSaleOrder($order_id) {

        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.id =:order_id')
                ->setParameters(array(
            'order_id' => $order_id
        ));
        $result = $query->getQuery()->getResult();
        return $result ? $result[0] : null;
    }

    /**
     * @return \Core\Entity\Order
     */
    public function getSaleOrder($order_id) {

        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.provider =:provider OR O.client =:client OR O.client IS NULL')
                ->andWhere('O.id =:order_id')
                ->setParameters(array(
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'order_id' => $order_id
        ));
        $result = $query->getQuery()->getResult();
        return $result ? $result[0] : null;
    }

    /**
     * @return \Core\Entity\InvoiceTax
     */
    public function getInvoiceTax($invoice_id) {

        $query = $this->_em->getRepository('\Core\Entity\InvoiceTax')
                ->createQueryBuilder('IT')
                ->select()
                ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.invoice_tax = IT.id')
                ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OIT.order')
                ->where('O.provider=:provider OR O.client=:client')
                ->andWhere('IT.id =:id')
                ->groupBy('IT.id')
                ->setParameters(array(
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'id' => $invoice_id,
        ));
        $result = $query->getQuery()->getResult();
        return $result ? $result[0] : null;
    }

    public function discoveryAAAAAAASOrderFromInvoiceTax($invoice, $issuer = NULL) {
        $nf = simplexml_load_string($invoice);
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $orderStatus = $shippingModel->discoveryOrderStatus('on the way', 'closed', 1, 1);
        $document = (string) $nf->CTe->infCte->emit->CNPJ;
        $issuer = $issuer ? : $this->_companyModel->getPeopleByDocument($document);
        if ($issuer) {
            $invoice_number = (string) $nf->CTe->infCte->ide->nCT;
            $invoice_tax = $this->_em->getRepository('\Core\Entity\InvoiceTax')
                            ->createQueryBuilder('IT')
                            ->select()
                            ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.invoice_tax = IT.id')
                            ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OIT.order')
                            ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.id = O.quote')
                            ->where('Q.carrier =:carrier')
                            ->andWhere('IT.invoice_number=:invoice_number')
                            ->andWhere('OIT.invoice_type =:invoice_type')
                            ->setParameters(array(
                                'invoice_number' => $invoice_number,
                                'carrier' => $issuer,
                                'invoice_type' => 57
                            ))->getQuery()->getResult();

            $invoice_tax = $invoice_tax ? $invoice_tax[0] : NULL;
            if (!$invoice_tax) {
                $invoice_tax = new \Core\Entity\InvoiceTax();
                $invoice_tax->setInvoice($invoice);
                $invoice_tax->setInvoiceNumber($invoice_number);
                $this->persist($invoice_tax);
                //$this->_em->flush($invoice_tax);
            }

            foreach ($nf->CTe->infCte->infCTeNorm->infDoc->infNFe AS $nf_key) {
                $nf_number = (int) substr((string) $nf_key->chave, 25, 9);
                if ($nf_number) {
                    $order = $this->_em->getRepository('\Core\Entity\Order')
                                    ->createQueryBuilder('O')
                                    ->select()
                                    ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.order = O.id')
                                    ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'IT.id = OIT.invoice_tax')
                                    ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.id = O.quote')
                                    ->andWhere('Q.carrier =:carrier')
                                    ->andWhere('IT.invoice_number =:invoice_number')
                                    ->andWhere('OIT.invoice_type =:invoice_type')
                                    ->setParameters(array(
                                        'invoice_number' => $nf_number,
                                        'carrier' => $issuer,
                                        'invoice_type' => 55
                                    ))->getQuery()->getResult();
                    $order = $order ? $order[0] : NULL;
                    if ($order) {
                        $orderInvoiceTax = $this->_em->getRepository('\Core\Entity\OrderInvoiceTax')->findOneBy(array(
                            'invoice_tax' => $invoice_tax,
                            'invoice_type' => 57,
                            'issuer' => $issuer,
                            'order' => $order
                        ));
                        if (!$orderInvoiceTax) {
                            $orderInvoiceTax = new \Core\Entity\OrderInvoiceTax();
                            $orderInvoiceTax->setInvoiceTax($invoice_tax);
                            $orderInvoiceTax->setInvoiceType(57);
                            $orderInvoiceTax->setIssuer($issuer);
                            $orderInvoiceTax->setOrder($order);
                            $this->persist($orderInvoiceTax);
                            //$this->_em->flush($orderInvoiceTax);
                        }
                        $this->addCarrierInvoiceTax($order, $invoice);
                    }
                }
            }
        }
    }

    public function discoveryOrderFromInvoiceTax(\Core\Entity\InvoiceTax $invoice_tax) {
        $nf = simplexml_load_string($invoice_tax->getInvoice());

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $orderStatus = $shippingModel->discoveryOrderStatus('on the way', 'closed', 1, 1);


        $document = (string) $nf->CTe->infCte->emit->CNPJ;

        $issuer = $this->_companyModel->getPeopleByDocument($document);
        if ($issuer) {
            foreach ($nf->CTe->infCte->infCTeNorm->infDoc->infNFe AS $nf_key) {
                $nf = (int) substr((string) $nf_key->chave, 25, 9);
                if ($nf) {
                    $order = $this->_em->getRepository('\Core\Entity\Order')
                                    ->createQueryBuilder('O')
                                    ->select()
                                    ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.order = O.id')
                                    ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'IT.id = OIT.invoice_tax')
                                    ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.id = O.quote')
                                    ->andWhere('Q.carrier =:carrier')
                                    ->andWhere('IT.invoice_number =:invoice_number')
                                    ->setParameters(array(
                                        'invoice_number' => $nf,
                                        'carrier' => $issuer
                                    ))->getQuery()->getResult();
                    $order = $order ? $order[0] : NULL;
                    if ($order) {
                        $orderInvoiceTax = $this->_em->getRepository('\Core\Entity\OrderInvoiceTax')->findOneBy(array(
                            'invoice_tax' => $invoice_tax,
                            'invoice_type' => 57,
                            'issuer' => $issuer,
                            'order' => $order
                        ));
                        if (!$orderInvoiceTax) {
                            $orderInvoiceTax = new \Core\Entity\OrderInvoiceTax();
                            $orderInvoiceTax->setInvoiceTax($invoice_tax);
                            $orderInvoiceTax->setInvoiceType(57);
                            $orderInvoiceTax->setIssuer($issuer);
                            $orderInvoiceTax->setOrder($order);
                            $this->persist($orderInvoiceTax);
                            //$this->_em->flush($orderInvoiceTax);
                            if ($order->getStatus()->getStatus() == 'waiting retrieve') {
                                $order->setStatus($orderStatus);
                                $this->persist($order);
                                //$this->_em->flush($order);
                            }
                        }
                    }
                }
            }
        }
    }

    public function createPurchasingInvoice(\Core\Entity\Order $order, \Core\Entity\People $provider) {

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);

        $invoice = $this->_em->getRepository('\Core\Entity\Invoice')
                        ->createQueryBuilder('I')
                        ->select()
                        ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.invoice = I.id')
                        ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OI.order')
                        ->where('(O.provider =:provider)')
                        ->andWhere('O.client =:client')
                        ->andWhere('I.invoice_status IN(:invoice_status)')
                        ->setParameters(array(
                            'invoice_status' => array(
							    $shippingModel->discoveryInvoiceStatus('open', 'pending', 1, 0),
								$shippingModel->discoveryInvoiceStatus('waiting payment', 'pending', 1, 1),
								$shippingModel->discoveryInvoiceStatus('outdated billing', 'pending', 1, 1)
							),
                            'provider' => $provider,
                            'client' => $this->_companyModel->getLoggedPeopleCompany()
                        ))->getQuery()->getResult();
        $invoice = $invoice ? $invoice[0] : NULL;

        if (!$invoice) {
            $date = new \DateTime('friday');
            $date->modify('+20 days');
            $invoice = new \Core\Entity\Invoice();
            $invoice->setPrice($order->getPrice());
            $invoice->setDueDate($date);
            $invoice->setStatus($shippingModel->discoveryInvoiceStatus('open', 'pending', 1, 1));
            $invoice->setPaymentDate(NULL);
        } else {
            $invoice->setPrice($invoice->getPrice() + $order->getPrice());
        }

        $this->persist($invoice);
        //$this->_em->flush($invoice);

        $orderInvoice = new \Core\Entity\OrderInvoice();
        $orderInvoice->setInvoice($invoice);
        $orderInvoice->setOrder($order);
        $this->persist($orderInvoice);
        //$this->_em->flush($orderInvoice);

        return $invoice;
    }

    public function createPurchasingOrderFromSaleOrder(\Core\Entity\Order $order, \Core\Entity\People $provider, $invoice) {										    
        $price = simplexml_load_string($invoice)->CTe->infCte->vPrest->vTPrest;
        if (!$price) {
            ErrorModel::addError('Impossible get price of DACTE');
            return;
        }

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $purchasingOrder = clone $order;
        $this->_em->detach($purchasingOrder);
        $purchasingOrder->resetId();
        $purchasingOrder->setClient($order->getQuote()->getProvider());
        $purchasingOrder->setPayer($order->getQuote()->getProvider());
		$purchasingOrder->setStatus($shippingModel->discoveryOrderStatus('delivered', 'closed', 1, 1));
        $purchasingOrder->setProvider($provider);
        $purchasingOrder->setPrice($price);
        $this->persist($purchasingOrder);
        //$this->_em->flush($purchasingOrder);

        $carrierInvoiceTax = $this->getCarrierInvoiceTaxFromOrder($order);
        if (!$carrierInvoiceTax) {

            $carrierInvoiceTax = new \Core\Entity\InvoiceTax();
            $carrierInvoiceTax->setInvoice($invoice);
            $this->persist($carrierInvoiceTax);
            //$this->_em->flush($carrierInvoiceTax);

            $purchasingOrderTax = new \Core\Entity\OrderInvoiceTax();
            $purchasingOrderTax->setInvoiceTax($carrierInvoiceTax);
            $purchasingOrderTax->setInvoiceType(57);
            $purchasingOrderTax->setIssuer($purchasingOrder->getProvider());
            $purchasingOrderTax->setOrder($purchasingOrder);
            $this->persist($purchasingOrderTax);
            //$this->_em->flush($purchasingOrderTax);

            $orderInvoiceTax = new \Core\Entity\OrderInvoiceTax();
            $orderInvoiceTax->setInvoiceTax($carrierInvoiceTax);
            $orderInvoiceTax->setInvoiceType(57);
            $orderInvoiceTax->setIssuer($purchasingOrder->getProvider());
            $orderInvoiceTax->setOrder($order);
            $this->persist($orderInvoiceTax);
            //$this->_em->flush($orderInvoiceTax);
        }

        $this->createPurchasingInvoice($purchasingOrder, $provider);
        return $carrierInvoiceTax;
    }

    public function addCarrierInvoiceTax(\Core\Entity\Order $order, $invoice) {

        $nf = simplexml_load_string($invoice);
		if (!$nf){
			return;
		}
        $price = $nf->CTe->infCte->vPrest->vTPrest;
        if (!$price) {
            ErrorModel::addError('Impossible get price of DACTE');
            return;
        }

        $clientInvoiceTax = $this->_em->getRepository('\Core\Entity\InvoiceTax')
                        ->createQueryBuilder('IT')
                        ->select()
                        ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.invoice_tax = IT.id')
                        ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OIT.order')
                        ->where('O.id =:order')
                        ->andWhere('OIT.invoice_type=:invoice_type')
                        ->setParameters(array(
                            'order' => $order->getId(),
                            'invoice_type' => 55
                        ))->getQuery()->getResult();

        if ($order->getStatus()->getStatus() == 'waiting retrieve') {
            foreach ($nf->CTe->infCte->infCTeNorm->infDoc->infNFe AS $nf_key) {				
                $nf_number = (int) substr((string) $nf_key->chave, 25, 9);
                $nfs[] = $nf_number;				
            }
            if ($clientInvoiceTax && in_array($clientInvoiceTax[0]->getInvoiceNumber(), $nfs)) {
                $shippingModel = new ShippingModel();
                $shippingModel->initialize($this->serviceLocator);
                $orderStatus = $shippingModel->discoveryOrderStatus('on the way', 'closed', 1, 1);
                $order->setStatus($orderStatus);
                $this->persist($order);
                //$this->_em->flush($order);
                $carrierInvoiceTax = $this->createPurchasingOrderFromSaleOrder($order, $order->getQuote()->getCarrier(), $invoice);
            } else {
                ErrorModel::addError('This invoice tax is not part of this order');
            }
        } else {
            ErrorModel::addError('Order is not in the correct status for sending invoice tax');
        }
        return $carrierInvoiceTax;
    }

    public function approveOrder($order_id) {

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $orderStatus = $shippingModel->discoveryOrderStatus('analysis', 'pending', 1, 0);

        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.provider =:provider OR O.client =:client OR O.client IS NULL')
                ->andWhere('O.id =:order_id')
                ->andWhere('O.order_status =:order_status')
                ->setParameters(array(
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'order_id' => $order_id,
            'order_status' => $orderStatus
        ));
        $result = $query->getQuery()->getResult();
        if (!$result) {
            ErrorModel::addError('Error on approve this order');
            return;
        } else {
            $this->createInvoiceFromOrder($result[0]);
        }
    }

    public function createInvoiceFromOrder(\Core\Entity\Order $order) {
        if ($order->getClient()->getId() != $order->getPayer()->getId() && $order->getProvider()->getId() == $order->getPayer()->getId()) {
            return $this->createSimpleInvoice($order);
        } else {
            return $this->createBillingInvoice($order);
        }
    }

    protected function createBillingInvoice(\Core\Entity\Order $order) {

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);

        if ($order->getPayer()->getBilling() > 0) {


            $outdated_invoice = $this->_em->getRepository('\Core\Entity\Invoice')
                            ->createQueryBuilder('I')
                            ->select()
                            ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.invoice = I.id')
                            ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OI.order')
                            ->where('(O.provider = :provider)')
                            ->andWhere('O.payer =:payer')
                            ->andWhere('I.invoice_status =:invoice_status')
                            ->setParameters(array(
                                'invoice_status' => $shippingModel->discoveryInvoiceStatus('outdated billing', 'pending', 1, 1),
                                'provider' => $this->_companyModel->getLoggedPeopleCompany(),
                                'payer' => $order->getPayer()
                            ))->getQuery()->getResult();
            $outdated_invoice = $outdated_invoice ? $outdated_invoice[0] : NULL;


            $invoice = $this->_em->getRepository('\Core\Entity\Invoice')
                            ->createQueryBuilder('I')
                            ->select()
                            ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.invoice = I.id')
                            ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OI.order')
                            ->where('(O.provider =:provider)')
                            ->andWhere('O.payer =:payer')
                            ->andWhere('I.invoice_status =:invoice_status')
                            ->setParameters(array(
                                'invoice_status' => $shippingModel->discoveryInvoiceStatus('open', 'pending', 1, 0),
                                'provider' => $this->_companyModel->getLoggedPeopleCompany(),
                                'payer' => $order->getPayer()
                            ))->getQuery()->getResult();
            $invoice = $invoice ? $invoice[0] : NULL;



            $exceeded_invoice = $this->_em->getRepository('\Core\Entity\Invoice')
                            ->createQueryBuilder('I')
                            ->select()
                            ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.invoice = I.id')
                            ->innerJoin('\Core\Entity\Order', 'O', 'WITH', 'O.id = OI.order')
                            ->where('(O.provider = :provider)')
                            ->andWhere('O.payer =:payer')
                            ->andWhere('I.invoice_status =:invoice_status')
                            ->setParameters(array(
                                'invoice_status' => $shippingModel->discoveryInvoiceStatus('exceeded billing', 'pending', 1, 1),
                                'provider' => $this->_companyModel->getLoggedPeopleCompany(),
                                'payer' => $order->getPayer()
                            ))->getQuery()->getResult();
            $exceeded_invoice = $exceeded_invoice ? $exceeded_invoice[0] : NULL;


            $date = new \DateTime('friday');
            $date->modify('+7 days');

            if ($invoice && !$exceeded_invoice && ($order->getPayer()->getBilling() < $order->getPrice() || ($invoice && $order->getPayer()->getBilling() < ($invoice->getPrice() + $order->getPrice())))) {
                $invoice->setStatus($shippingModel->discoveryInvoiceStatus('exceeded billing', 'pending', 1, 1));
                $this->persist($invoice);
                //$this->_em->flush($invoice);
                unset($invoice);
            }
            if (!$invoice) {
                $invoice = new \Core\Entity\Invoice();
                $invoice->setPrice($order->getPrice());
                $invoice->setDueDate($date);
                $invoice->setStatus($shippingModel->discoveryInvoiceStatus('open', 'pending', 1, 1));
                $invoice->setPaymentDate(NULL);
                $this->persist($invoice);
                //$this->_em->flush($invoice);
            }
            $invoicePrice = $this->getPriceFromInvoice($invoice);

            if (!$outdated_invoice && !$exceeded_invoice && $order->getPayer()->getBilling() <= $order->getPrice() && $order->getPayer()->getBilling() <= ($invoicePrice)) {
                $invoice->setPrice($invoicePrice);
                $this->persist($invoice);
                //$this->_em->flush($invoice);
                $order->setStatus($shippingModel->discoveryOrderStatus('waiting payment', 'open', 1, 1));

                $this->persist($order);
                //$this->_em->flush($order);
            } else {

                $order->setStatus($shippingModel->discoveryOrderStatus('waiting retrieve', 'pending', 1, 1));
                $this->persist($order);
                //$this->_em->flush($order);
				$companyModel = new CompanyModel();
				$companyModel->initialize($this->serviceLocator);		
				$state = $order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf();				
				$formID = Mautic::getFormID('retrieve-request',strtolower($state));
				$params['mauticform[formId]'] = $formID;	
                $params['mauticform[f_key]'] = Mautic::getOAuth2PublicKey();
                $params['mauticform[body]'] = Mail::renderMail($this->serviceLocator, 'retrieve-request.phtml', array(
                            'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                            'order' => $order
                ));
                $params['mauticform[email]'] = $order->getQuote()->getCarrier()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
                $client = new \Core\Helper\Api();
                $client->post(Mautic::getMauticFormAction($formID), array('form_params' => $params));
            }


            $orderInvoice = new \Core\Entity\OrderInvoice();
            $orderInvoice->setInvoice($invoice);
            $orderInvoice->setOrder($order);
            $this->persist($orderInvoice);
            //$this->_em->flush($orderInvoice);
        } else {
            return $this->createSimpleInvoice($order);
        }
    }

    public function getQuotesReport(\DateTime $start_date, \DateTime $end_date, $grouped = false) {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $query = $this->_em->createQueryBuilder()
                ->select('sum(O.price) AS price')
                ->addSelect('COUNT(O.id) AS total')
                ->from('\Core\Entity\Order', 'O')
                ->where('O.provider=:provider')
                ->andWhere('O.order_status IN(:status)')
                ->andWhere('O.order_date BETWEEN :start_date AND :end_date')
                ->setParameters(array(
					'provider' => $this->_companyModel->getLoggedPeopleCompany(),
					'start_date' => $start_date,
					'end_date' => $end_date,
					'status' => array(
						$shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
						$shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)
					),
				));
        if ($grouped) {
            $query->addSelect('date(O.order_date) AS date')->groupBy('date');
        } else {
            $query->setMaxResults(1);
        }

        return $query->getQuery()->getResult();
    }

    public function getSalesOrdersReport(\DateTime $start_date, \DateTime $end_date, $grouped = false) {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $subquery = $this->_em->createQueryBuilder()
                ->select('O.id')
                ->from('\Core\Entity\Order', 'O')
                ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.order = O.id')
                ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'OIT.invoice_tax = IT.id')
                ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OITP', 'WITH', 'IT.id = OITP.invoice_tax')
                ->innerJoin('\Core\Entity\Order', 'OP', 'WITH', 'OP.id = OITP.order')
                ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.order = O.id')
                ->where('OP.client=:client')
                ->andWhere('O.provider=:provider')
                ->andWhere('OIT.issuer = Q.carrier')
                ->andWhere('O.order_status NOT IN (:status)')
                ->andWhere('O.order_date BETWEEN :start_date AND :end_date')
                ->andWhere('OP.order_status NOT IN (:status)')
                ->groupBy('O.id');

        $query = $this->_em->createQueryBuilder()
                ->select('sum(OO.price) AS price')
                ->addSelect('COUNT(OO.id) AS total')
                ->from('\Core\Entity\Order', 'OO')
                ->where($this->_em->createQueryBuilder()->expr()->in('OO.id', $subquery->getDQL()))
                ->setParameters(array(
					'provider' => $this->_companyModel->getLoggedPeopleCompany(),
					'client' => $this->_companyModel->getLoggedPeopleCompany(),
					'start_date' => $start_date,
					'end_date' => $end_date,
					'status' => array(
						$shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
						$shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)
					),
				));
        if ($grouped) {
            $query->addSelect('date(OO.order_date) AS date')->groupBy('date');
        } else {
            $query->setMaxResults(1);
        }

        return $query->getQuery()->getResult();
    }

    public function getPurchasingOrdersReport(\DateTime $start_date, \DateTime $end_date, $grouped = false) {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $subquery = $this->_em->createQueryBuilder()
                ->select('O.id')
                ->from('\Core\Entity\Order', 'O')
                ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OIT', 'WITH', 'OIT.order = O.id')
                ->innerJoin('\Core\Entity\InvoiceTax', 'IT', 'WITH', 'OIT.invoice_tax = IT.id')
                ->innerJoin('\Core\Entity\OrderInvoiceTax', 'OITS', 'WITH', 'IT.id = OITS.invoice_tax')
                ->innerJoin('\Core\Entity\Order', 'OS', 'WITH', 'OS.id = OITS.order')
                ->innerJoin('\Core\Entity\Quote', 'Q', 'WITH', 'Q.order = OS.id')    
				
				//->leftJoin('\Core\Entity\Order', 'CO', 'WITH', 'CO.quote = O.quote AND CO.provider =:provider')
				
                ->where('O.client=:client')															
				->andWhere('OIT.issuer = Q.carrier')				
                ->andWhere('O.order_status NOT IN (:status)')
                ->andWhere('OS.order_date BETWEEN :start_date AND :end_date')
                ->andWhere('OS.order_status NOT IN (:status)')
				->groupBy('O.id');

        $query = $this->_em->createQueryBuilder()
                ->select('sum(OO.price) AS price')
                ->addSelect('COUNT(OO.id) AS total')
                ->from('\Core\Entity\Order', 'OO')
                ->where($this->_em->createQueryBuilder()->expr()->in('OO.id', $subquery->getDQL()))
                ->setParameters(array(		
					//'provider' => $this->_companyModel->getMainDefaultCompany(),
					'client' => $this->_companyModel->getLoggedPeopleCompany(),
					'start_date' => $start_date,
					'end_date' => $end_date,
					'status' => array(
						$shippingModel->discoveryOrderStatus('quote', 'closed', 1, 0),
						$shippingModel->discoveryOrderStatus('canceled', 'canceled', 1, 1)
					),
				));

        if ($grouped) {
            $query->addSelect('date(OO.order_date) AS date')->groupBy('date');
        } else {
            $query->setMaxResults(1);
        }
        return $query->getQuery()->getResult();
    }

    public function getOrdersReport($start_date = NULL, $end_date = NULL) {

        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        if (!$start_date) {
            $start_date = new \DateTime();
            $start_date->modify('-30 days');
        } else {
            $start_date = new \DateTime($start_date);
        }
        if (!$end_date) {
            $end_date = new \DateTime();
            $end_date->modify('last day of this month');
            $end_date->modify('+1 day');
        } else {
            $end_date = new \DateTime($end_date);
            $end_date->modify('+1 day');
        }

        $saleOrders = $this->getSalesOrdersReport($start_date, $end_date);
        $quotesOrders = $this->getQuotesReport($start_date, $end_date);
        $purchasingOrders = $this->getPurchasingOrdersReport($start_date, $end_date);

        $sales = $this->getSalesOrdersReport($start_date, $end_date, true);
        $quotes = $this->getQuotesReport($start_date, $end_date, true);
        $purchasings = $this->getPurchasingOrdersReport($start_date, $end_date, true);
        $translateModel = new \Translate\Model\TranslateModel();
        $translateModel->initialize($this->serviceLocator);

        $price_series[0]['name'] = $translateModel->translateFromKey('Quotes')->getTranslate();
        $total_series[0]['name'] = $translateModel->translateFromKey('Quotes')->getTranslate();
        foreach ($quotes AS $key => $quote) {
            $categories[$quote['date']] = date('d/m/Y', strtotime($quote['date']));
            $price_series[0]['data'][$quote['date']] = (float) $quote['price'];
            $total_series[0]['data'][$quote['date']] = (float) $quote['total'];
            $price_series[1]['data'][$quote['date']] = (float) 0;
            $total_series[1]['data'][$quote['date']] = (float) 0;
            $price_series[2]['data'][$quote['date']] = (float) 0;
            $total_series[2]['data'][$quote['date']] = (float) 0;
        }

        $price_series[2]['name'] = $translateModel->translateFromKey('Sales Order')->getTranslate();
        $total_series[2]['name'] = $translateModel->translateFromKey('Sales Order')->getTranslate();
        foreach ($sales AS $key => $sale) {
            $categories[$sale['date']] = date('d/m/Y', strtotime($sale['date']));
            $price_series[2]['data'][$sale['date']] = (float) $sale['price'];
            $total_series[2]['data'][$sale['date']] = (float) $sale['total'];
            $price_series[1]['data'][$sale['date']] = (float) $price_series[1]['data'][$sale['date']] ? : 0;
            $total_series[1]['data'][$sale['date']] = (float) $total_series[1]['data'][$sale['date']] ? : 0;
            $price_series[0]['data'][$sale['date']] = (float) $price_series[0]['data'][$sale['date']] ? : 0;
            $total_series[0]['data'][$sale['date']] = (float) $total_series[0]['data'][$sale['date']] ? : 0;
        }

        $price_series[1]['name'] = $translateModel->translateFromKey('Purchasing Order')->getTranslate();
        $total_series[1]['name'] = $translateModel->translateFromKey('Purchasing Order')->getTranslate();
        foreach ($purchasings AS $key => $purchasing) {
            $categories[$purchasing['date']] = date('d/m/Y', strtotime($purchasing['date']));
            $price_series[1]['data'][$purchasing['date']] = (float) $purchasing['price'];
            $total_series[1]['data'][$purchasing['date']] = (float) $purchasing['total'];
            $price_series[0]['data'][$purchasing['date']] = (float) $price_series[0]['data'][$purchasing['date']] ? : 0;
            $total_series[0]['data'][$purchasing['date']] = (float) $total_series[0]['data'][$purchasing['date']] ? : 0;
            $price_series[2]['data'][$purchasing['date']] = (float) $price_series[2]['data'][$purchasing['date']] ? : 0;
            $total_series[2]['data'][$purchasing['date']] = (float) $total_series[2]['data'][$purchasing['date']] ? : 0;
        }
        $price_series[0]['data'] = array_values($price_series[0]['data']);
        $price_series[2]['data'] = array_values($price_series[2]['data']);
        $price_series[1]['data'] = array_values($price_series[1]['data']);
        $total_series[0]['data'] = array_values($total_series[0]['data']);
        $total_series[2]['data'] = array_values($total_series[2]['data']);
        $total_series[1]['data'] = array_values($total_series[1]['data']);
        $categories = array_values($categories);

        return array(
            'quotes_month' => $quotesOrders[0]['total'],
            'quotes_total_month' => $quotesOrders[0]['price'],
            'sales_month' => $saleOrders[0]['total'],
            'sales_total_month' => $saleOrders[0]['price'],
            'purchasing_total_month' => $purchasingOrders[0]['price'],
            'start_date' => $start_date,
            'end_date' => $end_date->modify('-1 day'),
            'report_details' => array(
                'categories' => $categories,
                'price_series' => $price_series,
                'total_series' => $total_series
            )
        );
    }

    public function getPriceFromInvoice(\Core\Entity\Invoice $invoice) {
        $price = $this->_em->createQueryBuilder()->select('sum(O.price)')
                        ->from('\Core\Entity\Order', 'O')
                        ->innerJoin('\Core\Entity\OrderInvoice', 'OI', 'WITH', 'OI.order = O.id')
                        ->where('OI.invoice=:invoice')
                        ->setParameters(array(
                            'invoice' => $invoice
                        ))
                        ->setMaxResults(1)->getQuery()->getResult();

        return $price ? $price[0][2] : 0;
    }

    protected function createSimpleInvoice(\Core\Entity\Order $order) {
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $date = new \DateTime();
        $date->modify('+7 days');
        $invoice = new \Core\Entity\Invoice();
        $invoice->setPrice($order->getPrice());
        $invoice->setDueDate($date);
        $invoice->setStatus($shippingModel->discoveryInvoiceStatus('waiting payment', 'pending', 1, 1));
        $invoice->setPaymentDate(NULL);
        $orderInvoice = new \Core\Entity\OrderInvoice();
        $orderInvoice->setInvoice($invoice);
        $orderInvoice->setOrder($order);
        $this->persist($invoice);
        //$this->_em->flush($invoice);
        $this->persist($orderInvoice);
        //$this->_em->flush($orderInvoice);
        $order->setStatus($shippingModel->discoveryOrderStatus('waiting payment', 'pending', 1, 1));
        $this->persist($order);
        //$this->_em->flush($order);
        $orderModel = new OrderModel();
        $financeModel = new \Finance\Model\FinanceModel();
        $financeModel->initialize($this->serviceLocator);
		
		
		$companyModel = new CompanyModel();
		$companyModel->initialize($this->serviceLocator);		
		$state = $order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf();
		$formID = Mautic::getFormID('invoice-payment',strtolower($state));
		$params['mauticform[formId]'] = $formID;							        
        $params['mauticform[f_key]'] = Mautic::getOAuth2PublicKey();
        $params['mauticform[body]'] = Mail::renderMail($this->serviceLocator, 'client-invoice.phtml', array(
                    'saleOrders' => $financeModel->getSalesOrdersFromSaleInvoice($invoice->getId()),
                    'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                    'order' => $order,
                    'itauData' => $orderModel->getItauDataFromOrder($order)
        ));
        if ($order->getPayer()->getPeopleType() == 'J') {
            $params['mauticform[email]'] = $order->getPayer()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
        } else {
            $params['mauticform[email]'] = $order->getPayer()->getEmail()[0]->getEmail();
        }
        $client = new \Core\Helper\Api();
        $client->post(Mautic::getMauticFormAction($formID), array('form_params' => $params));
        return $orderInvoice;
    }

    /**
     * @return \Core\Entity\Order
     */
    public function getPurchasingOrder($order_id) {

        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.provider =:provider OR O.client =:client OR O.client IS NULL')
                ->andWhere('O.id =:order_id')
                ->setParameters(array(
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'order_id' => $order_id
        ));
        $result = $query->getQuery()->getResult();
        return $result ? $result[0] : null;
    }

    public function getSaleOrders($params, $limit = 50, $offset = 0) {
        if ($params['order-status']) {
            $real_order_status = $this->_em->getRepository('\Core\Entity\OrderStatus')->find($params['order-status']);
        } else {
            $real_order_status = $this->_em->getRepository('\Core\Entity\OrderStatus')->findBy(array('real_status' => array('open', 'pending')));
        }
        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.provider =:provider')
                ->andWhere('O.order_status IN(:real_order_status)')
                ->andWhere('O.client IS NOT NULL')
                ->setMaxResults($limit)
                ->setFirstResult($offset)
                ->orderBy('O.order_date', 'DESC')
                ->setParameters(array(
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'real_order_status' => $real_order_status
        ));
        return $query->getQuery()->getResult();
    }

    public function getAllOrderStatus() {
        return $this->_em->getRepository('\Core\Entity\OrderStatus')->findAll();
    }

    /**
     * @return \Core\Entity\Quote
     */
    public function getAllQuotesFromOrder($id) {

        $query = $this->_em->getRepository('\Core\Entity\Quote')
                ->createQueryBuilder('Q')
                ->select()
                ->where('(Q.provider =:provider OR Q.client =:client OR Q.client IS NULL)')
                ->andWhere('Q.order =:id')
                ->setParameters(array(
            'id' => $id,
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany()
        ));
        return $query->getQuery()->getResult();
    }

    public function getPurchasingOrders($params, $limit = 50, $offset = 0) {
        if ($params['order-status']) {
            $real_order_status = $this->_em->getRepository('\Core\Entity\OrderStatus')->find($params['order-status']);
        } else {
            $real_order_status = $this->_em->getRepository('\Core\Entity\OrderStatus')->findBy(array('real_status' => array('open', 'pending')));
        }
        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('O.client =:client')
                ->andWhere('O.order_status IN(:real_order_status)')
                ->setMaxResults($limit)
                ->setFirstResult($offset)
                ->orderBy('O.order_date', 'DESC')
                ->setParameters(array(
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'real_order_status' => $real_order_status
        ));
        return $query->getQuery()->getResult();
    }

}
