<?php

namespace Sales\Model;

use Core\Model\DefaultModel;
use Core\Model\ErrorModel;
use Core\Helper\Format;
use Company\Model\CompanyModel;
use Core\Model\AddressModel;
use Sales\Model\ShippingModel;

class QuoteModel extends DefaultModel {

    /**
     * @var \Company\Model\CompanyModel $entity   
     */
    protected $_companyModel;

    public function initialize(\Zend\ServiceManager\ServiceManager $serviceLocator) {
        parent::initialize($serviceLocator);
        $this->_companyModel = new CompanyModel();
        $this->_companyModel->initialize($serviceLocator);
    }

    public function discoveryClientContact($params) {
        if ($params['contact-id']) {
            $contact = $this->_em->getRepository('\Core\Entity\People')->find($params['contact-id']);
        }
        $email = $this->_em->getRepository('\Core\Entity\Email')->findOneBy(array(
            'email' => $params['email']
        ));
        $phone = $this->_em->getRepository('\Core\Entity\Phone')->findOneBy(array(
            'phone' => $params['phone'],
            'ddd' => $params['ddd']
        ));
        if (!$contact) {
            $query = $this->_em->getRepository('\Core\Entity\People')
                    ->createQueryBuilder('P')
                    ->select()
                    ->innerJoin('\Core\Entity\Email', 'E', 'WITH', 'E.people = P.id')
                    ->where('E.email =:email')
                    ->setParameters(array(
                'email' => $params['email']
            ));
            $contacts = $query->getQuery()->getResult();
            $contact = $contacts ? $contacts[0] : null;
        }
        /*
          if (!$contact) {
          $query = $this->_em->getRepository('\Core\Entity\People')
          ->createQueryBuilder('P')
          ->select()
          ->innerJoin('\Core\Entity\Phone', 'T', 'WITH', 'T.people = P.id')
          ->where('T.phone =:phone AND T.ddd =:ddd')
          ->setParameters(array(
          'phone' => $params['phone'],
          'ddd' => $params['ddd']
          ));
          $contacts = $query->getQuery()->getResult();
          $contact = $contacts ? $contacts[0] : null;
          }
         */

        if (!$contact) {
            $contact = new \Core\Entity\People();
            $contact->setName($params['contact-name']);
            $contact->setEnabled(1);
            $contact->setAlias('');
            $contact->setPeopleType('F');
            $contact->setLanguage($this->_companyModel->getLoggedPeopleCompany()->getLanguage());
            $this->persist($contact);
            //$this->_em->flush($contact);
        }
        if (!$email) {
            $email = new \Core\Entity\Email();
            $email->setEmail($params['email']);
            $email->setConfirmed(0);
            $email->setPeople($contact);
            $this->persist($email);
            //$this->_em->flush($email);
        }
        if (!$phone) {
            $phone = new \Core\Entity\Phone();
            $phone->setConfirmed(0);
            $phone->setDdd($params['ddd']);
            $phone->setPhone($params['phone']);
            $phone->setPeople($contact);
            $this->persist($phone);
            //$this->_em->flush($phone);
        }
        return $contact;
    }

    public function discoveryCompanyContact(\Core\Entity\People $people, $params) {
        if ($params['contact-id']) {
            $contact = $this->_em->getRepository('\Core\Entity\People')->find($params['contact-id']);
        }
        $email = $this->_em->getRepository('\Core\Entity\Email')->findOneBy(array(
            'email' => $params['email']
        ));
        $phone = $this->_em->getRepository('\Core\Entity\Phone')->findOneBy(array(
            'phone' => $params['phone'],
            'ddd' => $params['ddd']
        ));
        if (!$contact) {
            $query = $this->_em->getRepository('\Core\Entity\People')
                    ->createQueryBuilder('P')
                    ->select()
                    ->innerJoin('\Core\Entity\Email', 'E', 'WITH', 'E.people = P.id')
                    ->where('E.email =:email')
                    ->setParameters(array(
                'email' => $params['email']
            ));
            $contacts = $query->getQuery()->getResult();
            $contact = $contacts ? $contacts[0] : null;
        }
        if (!$contact) {
            $query = $this->_em->getRepository('\Core\Entity\People')
                    ->createQueryBuilder('P')
                    ->select()
                    ->innerJoin('\Core\Entity\Phone', 'T', 'WITH', 'T.people = P.id')
                    ->where('T.phone =:phone AND T.ddd =:ddd')
                    ->setParameters(array(
                'phone' => $params['phone'],
                'ddd' => $params['ddd']
            ));
            $contacts = $query->getQuery()->getResult();
            $contact = $contacts ? $contacts[0] : null;
        }
        if (!$contact) {
            $contact = new \Core\Entity\People();
            $contact->setName($params['contact-name']);
            $contact->setEnabled(1);
            $contact->setAlias('');
            $contact->setPeopleType('F');
            $contact->setLanguage($this->_companyModel->getLoggedPeopleCompany()->getLanguage());
            $this->persist($contact);
            //$this->_em->flush($contact);
        }
        if (!$email) {
            $email = new \Core\Entity\Email();
            $email->setEmail($params['email']);
            $email->setConfirmed(0);
            $email->setPeople($contact);
            $this->persist($email);
            //$this->_em->flush($email);
        }
        if (!$phone) {
            $phone = new \Core\Entity\Phone();
            $phone->setConfirmed(0);
            $phone->setDdd($params['ddd']);
            $phone->setPhone($params['phone']);
            $phone->setPeople($contact);
            $this->persist($phone);
            //$this->_em->flush($phone);
        }

        $peopleEmployee = $this->_em->getRepository('\Core\Entity\PeopleEmployee')->findOneBy(array(
            'company' => $people,
            'employee' => $contact
        ));

        if (!$peopleEmployee) {
            $peopleEmployee = new \Core\Entity\PeopleEmployee();
            $peopleEmployee->setCompany($people);
            $peopleEmployee->setEmployee($contact);
            $this->persist($peopleEmployee);
            //$this->_em->flush($peopleEmployee);
            $people->addPeopleEmployee($peopleEmployee);
            $this->persist($people);
            //$this->_em->flush($people);
        }

        return $contact;
    }

    protected function checkQuoteData($params) {

        if (!$params['contact-name']) {
            ErrorModel::addError('Contact name is required');
        }
        if (!$params['email']) {
            ErrorModel::addError('Email is required');
        }
        if (!$params['ddd']) {
            ErrorModel::addError('DDD is required');
        } else {
            $params['ddd'] = Format::onlyNumbers($params['ddd']);
        }
        if (!$params['phone']) {
            ErrorModel::addError('Phone is required');
        } else {
            $params['phone'] = Format::onlyNumbers($params['phone']);
        }
        return $params;
    }

    public function addQuoteOriginCompany($params) {
        $data = $this->checkQuoteData($params);
        if ($data) {
            $contact = $this->discoveryCompanyContact($this->_companyModel->getLoggedPeopleCompany(), $data);
            $quote = $this->_em->getRepository('\Core\Entity\Order')->findOneBy(array('quote' => $data['quote'], 'client' => $this->_companyModel->getLoggedPeopleCompany()));
            $addressModel = new AddressModel();
            $addressModel->initialize($this->serviceLocator);
            $address = $addressModel->addPeopleAddress($this->_companyModel->getLoggedPeopleCompany(), $data);
            if ($quote && $contact && $address) {
                $quote->setAddressOrigin($address);
                $quote->setRetrieveContact($contact);
                $quote->setRetrievePeople($this->_companyModel->getLoggedPeopleCompany());
                $this->persist($quote);
                //$this->_em->flush($quote);
            }
            if (!$quote) {
                ErrorModel::addError('Quote not found');
            }
        }
        return $quote;
    }

    public function addQuoteOriginClient($params) {
        $data = $this->checkQuoteData($params);
        $contact = $this->discoveryClientContact($data);
        $client = $this->discoveryClient($data, $contact);
        if ($contact && $data && $client) {
            $quote = $this->_em->getRepository('\Core\Entity\Order')->findOneBy(array('quote' => $data['quote'], 'client' => $this->_companyModel->getLoggedPeopleCompany()));
            $addressModel = new AddressModel();
            $addressModel->initialize($this->serviceLocator);
            $address = $addressModel->addPeopleAddress($client, $data);
            if ($quote && $contact && $address) {
                $quote->setAddressOrigin($address);
                $quote->setRetrieveContact($contact);
                $quote->setRetrievePeople($client);
                $this->persist($quote);
                //$this->_em->flush($quote);
            }
            if (!$quote) {
                ErrorModel::addError('Quote not found');
            }
        }
        return $quote;
    }

    public function addQuoteDestinationCompany($params) {
        $data = $this->checkQuoteData($params);
        if ($data) {
            $contact = $this->discoveryCompanyContact($this->_companyModel->getLoggedPeopleCompany(), $data);
            $quote = $this->_em->getRepository('\Core\Entity\Order')->findOneBy(array('quote' => $data['quote'], 'client' => $this->_companyModel->getLoggedPeopleCompany()));
            $addressModel = new AddressModel();
            $addressModel->initialize($this->serviceLocator);
            $address = $addressModel->addPeopleAddress($this->_companyModel->getLoggedPeopleCompany(), $data);
            if ($quote && $contact && $address) {
                $quote->setDeliveryContact($contact);
                $quote->setDeliveryPeople($this->_companyModel->getLoggedPeopleCompany());
                $quote->setAddressDestination($address);
                $this->persist($quote);
                //$this->_em->flush($quote);
            }
            if (!$quote) {
                ErrorModel::addError('Quote not found');
            }
        }
        return $quote;
    }

    public function addQuoteDestinationClient($params) {
        $data = $this->checkQuoteData($params);
        $contact = $this->discoveryClientContact($data);
        $client = $this->discoveryClient($data, $contact);
        if ($contact && $data && $client) {


            $shippingModel = new ShippingModel();
            $shippingModel->initialize($this->serviceLocator);
            $order = $shippingModel->getOrderFromQuote($data['quote']);

            $addressModel = new AddressModel();
            $addressModel->initialize($this->serviceLocator);
            $address = $addressModel->addPeopleAddress($client, $data);
            if ($order && $contact && $address) {
                $order->setDeliveryContact($contact);
                $order->setDeliveryPeople($client);
                $order->setAddressDestination($address);
                $this->persist($order);
                //$this->_em->flush($order);
            }
            if (!$order) {
                ErrorModel::addError('Quote not found');
            }
        }
        return $quote;
    }

    public function discoveryClient($params, $contact) {
        if ($params['choose-client'] == 'company') {
            $documentType = $this->_em->getRepository('\Core\Entity\DocumentType')->findOneBy(array(
                'documentType' => 'CNPJ'
            ));
            $document = $this->_em->getRepository('\Core\Entity\Document')->findOneBy(array(
                'document' => Format::onlyNumbers($params['company-document']),
                'documentType' => $documentType
            ));

            $people = $document ? $document->getPeople() : $this->_em->getRepository('\Core\Entity\People')->findOneBy(array(
                        'alias' => $params['company-alias'],
                        'name' => $params['company-name']
            ));
            if (!$people) {
                $people = new \Core\Entity\People();
                $people->setAlias($params['company-alias']);
                $people->setName($params['company-name']);
                $people->setEnabled(0);
                $people->setPeopleType('J');
                $people->setImage();
                $people->setLanguage($this->_companyModel->getLoggedPeopleCompany()->getLanguage());
                $this->persist($people);
                //$this->_em->flush($people);
                if ($params['company-document']) {
                    $document = new \Core\Entity\Document();
                    $document->setImage();
                    $document->setDocument(Format::onlyNumbers($params['company-document']));
                    $document->setDocumentType($documentType);
                    $document->setPeople($people);
                    $this->persist($document);
                    //$this->_em->flush($document);
                }
            }
            $peopleEmployee = $this->_em->getRepository('\Core\Entity\PeopleEmployee')->findOneBy(array(
                'company' => $people,
                'employee' => $contact
            ));
            if (!$peopleEmployee) {
                $peopleEmployee = new \Core\Entity\PeopleEmployee();
                $peopleEmployee->setCompany($people);
                $peopleEmployee->setEmployee($contact);
                $this->persist($peopleEmployee);
                //$this->_em->flush($peopleEmployee);
                $people->addPeopleEmployee($peopleEmployee);
                $this->persist($people);
                //$this->_em->flush($people);
            }
        } elseif ($params['choose-client'] == 'client') {
            $documentType = $this->_em->getRepository('\Core\Entity\DocumentType')->findOneBy(array(
                'documentType' => 'CPF'
            ));
            $document = $this->_em->getRepository('\Core\Entity\Document')->findOneBy(array(
                'document' => Format::onlyNumbers($params['client-document']),
                'documentType' => $documentType
            ));


            $documentPeopleType = $this->_em->getRepository('\Core\Entity\Document')->findOneBy(array(
                'documentType' => $documentType,
                'people' => $contact
            ));

            //if ($documentPeopleType && $documentPeopleType->getDocument() != Format::onlyNumbers($params['client-document'])) {
            //    ErrorModel::addError('This email is already registered in our system with another document');
            //    return;
            //} elseif ($document && $document->getPeople()->getId() != $contact->getId()) {
            //    ErrorModel::addError('This document is already registered in our system with another email');
            //    return;
            //} else
	    if (!$document && $params['client-document']) {
                $document = new \Core\Entity\Document();
                $document->setImage();
                $document->setDocument(Format::onlyNumbers($params['client-document']));
                $document->setDocumentType($documentType);
                $document->setPeople($contact);
                $this->persist($document);
                //$this->_em->flush($document);
            }
            $people = $contact;
        }

        $peopleClient = $this->_em->getRepository('\Core\Entity\PeopleClient')->findOneBy(array(
            'company' => $this->_companyModel->getLoggedPeopleCompany(),
            'client' => $people
        ));

        if (!$peopleClient) {
            $peopleClient = new \Core\Entity\PeopleClient();
            $peopleClient->setCompany($this->_companyModel->getLoggedPeopleCompany());
            $peopleClient->setClient($people);
            $this->persist($peopleClient);
            //$this->_em->flush($peopleClient);

            $people->addPeopleClient($peopleClient);
            $this->persist($people);
            //$this->_em->flush($people);
        }
        return $people;
    }

}
