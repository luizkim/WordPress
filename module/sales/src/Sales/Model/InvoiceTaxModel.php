<?php

namespace Sales\Model;

use Core\Model\DefaultModel;
use NFePHP\NFe\Make;
use Sales\Model\OrderModel;

class InvoiceTaxModel extends DefaultModel {

    /**
     * @var \NFePHP\NFe\Make $_nfe   
     */
    protected $_nfe;

    /**
     * @var \Core\Entity\Order $_order   
     */
    protected $_order;

    /**
     * @var \Sales\Model\OrderModel $_orderModel   
     */
    protected $_orderModel;

    public function initialize(\Zend\ServiceManager\ServiceManager $serviceLocator) {
        parent::initialize($serviceLocator);
        $this->_nfe = new Make();
        $this->_orderModel = new OrderModel();
        $this->_orderModel->initialize($serviceLocator);
    }

    public function createInvoiceTaxFromOrder($order_id) {
        $this->_order = $this->_orderModel->getSaleOrder($order_id);
        $this->getTagEmit();
        $this->getTagIde();
        $this->getTaginfNFe();
        $this->getTagEnderEmit();
        $this->getTagDest();
        $this->getTagEnderDest();
        $this->getTagEnderRetirada();
        $this->getTagEnderEntrega();
        $this->getTagTransp();
        $this->getTagEnderTransportadora();
        $this->getTagProd();
        //$this->getTagImposto();
        try {
            header("Content-type: text/xml");
            echo $this->_nfe->getXML();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    protected function getTaginfNFe() {
        $std = new \stdClass();
        $std->versao = '4.0'; //versão do layout        
        $std->pk_nItem = null; //deixe essa variavel sempre como NULL
        $std->Id = null;
        return $this->_nfe->taginfNFe($std);
    }

    protected function getTagIde() {

        $std = new \stdClass();
        $std->nNF = 2; //Número do Documento Fiscal. 
        $std->cMunFG = 3518800;
        $std->cUF = 35;
        $std->dhEmi = '2015-02-19T13:48:00-02:00';




        $std->cNF = rand(10000000, 99999999); //Código numérico que compõe a Chave de Acesso. Número aleatório gerado pelo emitente para cada NF-e para evitar acessos indevidos da NF-e.          
        $std->natOp = 'VENDA';
        //$std->indPag = 0; //NÃO EXISTE MAIS NA VERSÃO 4.00 
        $std->mod = 55;
        //$std->serie = 1; //Série do Documento Fiscal, preencher com zeros na hipótese de a NF-e não possuir série. (v2.0) Série 890-899 de uso exclusivo para emissão de NF-e avulsa, pelo contribuinte com seu certificado digital, através do site do Fisco (procEmi=2). (v2.0) Serie 900-999 – uso exclusivo de NF-e emitidas no SCAN. (v2.0)                
        $std->dhSaiEnt = null;
        $std->tpNF = 1;
        $std->idDest = 1;
        $std->tpImp = 1;
        $std->tpEmis = 1;
        $std->tpAmb = 1; // Identificação do Ambiente: 1 – Produção / 2 - Homologação
        $std->finNFe = 1;
        $std->indFinal = 0;
        $std->indPres = 0;
        $std->procEmi = 0;
        $std->verProc = 'v1.0.0';
        $std->dhCont = null;
        $std->xJust = null;

        return $this->_nfe->tagide($std);
    }

    protected function getTagProd() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe        
        $std->xProd = 'Intermediação de Negócios';
        $std->cProd = 'CFOP';
        $std->NCM = 99; //Código NCM (8 posições), informar o gênero (posição do capítulo do NCM) quando a operação não for de comércio exterior (importação/ exportação) ou o produto não seja tributado pelo IPI. Em caso de serviço informar o código 99 (v2.0)


        $std->CFOP;
        $std->cBenf; //incluido no layout 4.00                                
        $std->vProd = $this->_order->getPrice();
        $std->uCom; //Unidade Comercial do produto
        $std->qCom = 1;
        $std->vUnCom = $this->_order->getPrice(); //Valor Unitário de Comercialização do produto
        $std->qTrib = 10;
        //$std->vFrete = 50;
        //$std->vSeg = 30;
        $std->vUnTrib = 10; //Valor Unitário de tributação do produto
        $std->uTrib = 10; //Unidade Tributável do produto
        $std->indTot = $this->_order->getPrice();
        $std->xPed = $this->_order->getId();

        return $this->_nfe->tagprod($std);
    }

    protected function getTagImposto() {
        $std = new stdClass();
        $std->item = 1; //item da NFe
        $std->vTotTrib = 1000.00;
        return $this->_nfe->tagimposto($std);
    }

    protected function getTagTransp() {
        $std = new \stdClass();
        $std->modFrete = 0;
        return $this->_nfe->tagtransp($std);
    }

    protected function getTagEnderTransportadora() {
        $std = new \stdClass();
        $std->xNome = $this->_order->getQuote()->getCarrier()->getName();
        $std->xEnder = $this->_order->getQuote()->getCarrier()->getAddress()[0]->getStreet()->getStreet() . ', ' . $this->_order->getQuote()->getCarrier()->getAddress()[0]->getNumber() . ($this->_order->getQuote()->getCarrier()->getAddress()[0]->getComplement() ? (' - ' . $this->_order->getQuote()->getCarrier()->getAddress()[0]->getComplement()) : '');
        $std->xMun = $this->_order->getQuote()->getCarrier()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getCity();
        $std->UF = $this->_order->getQuote()->getCarrier()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getUf();
        foreach ($this->_order->getQuote()->getCarrier()->getDocument() AS $document) {
            $std->CNPJ = $document->getDocumentType()->getDocumentType() == 'CNPJ' ? $document->getDocument() : '';
            $std->IE = $document->getDocumentType()->getDocumentType() == 'Inscrição Estadual' ? $document->getDocument() : '';
            $std->CPF = $document->getDocumentType()->getDocumentType() == 'CPF' ? $document->getDocument() : '';
        }
        return $this->_nfe->tagtransporta($std);
    }

    protected function getTagEnderEntrega() {
        $std = new \stdClass();
        $std->xLgr = $this->_order->getDeliveryPeople()->getAddress()[0]->getStreet()->getStreet();
        $std->nro = $this->_order->getDeliveryPeople()->getAddress()[0]->getNumber();
        $std->xCpl = $this->_order->getDeliveryPeople()->getAddress()[0]->getComplement();
        $std->xBairro = $this->_order->getDeliveryPeople()->getAddress()[0]->getStreet()->getDistrict()->getDistrict();
        $std->cMun = NULL;
        $std->xMun = $this->_order->getDeliveryPeople()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getCity();
        $std->UF = $this->_order->getDeliveryPeople()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getUf();
        $std->CEP = $this->_order->getDeliveryPeople()->getAddress()[0]->getStreet()->getCep()->getCep();

        foreach ($this->_order->getDeliveryPeople()->getDocument() AS $document) {
            $std->CNPJ = $document->getDocumentType()->getDocumentType() == 'CNPJ' ? $document->getDocument() : '';
            $std->CPF = $document->getDocumentType()->getDocumentType() == 'CPF' ? $document->getDocument() : '';
        }

        return $this->_nfe->tagentrega($std);
    }

    protected function getTagEnderRetirada() {
        $std = new \stdClass();
        $std->xLgr = $this->_order->getRetrievePeople()->getAddress()[0]->getStreet()->getStreet();
        $std->nro = $this->_order->getRetrievePeople()->getAddress()[0]->getNumber();
        $std->xCpl = $this->_order->getRetrievePeople()->getAddress()[0]->getComplement();
        $std->xBairro = $this->_order->getRetrievePeople()->getAddress()[0]->getStreet()->getDistrict()->getDistrict();
        $std->cMun = NULL;

        $std->xMun = $this->_order->getRetrievePeople()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getCity();
        $std->UF = $this->_order->getRetrievePeople()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getUf();
        $std->CEP = $this->_order->getRetrievePeople()->getAddress()[0]->getStreet()->getCep()->getCep();

        foreach ($this->_order->getRetrievePeople()->getDocument() AS $document) {
            $std->CNPJ = $document->getDocumentType()->getDocumentType() == 'CNPJ' ? $document->getDocument() : '';
            $std->CPF = $document->getDocumentType()->getDocumentType() == 'CPF' ? $document->getDocument() : '';
        }

        return $this->_nfe->tagretirada($std);
    }

    protected function getTagDest() {
        $std = new \stdClass();
        $std->xNome = $this->_order->getClient()->getName();
        foreach ($this->_order->getClient()->getDocument() AS $document) {
            $std->CNPJ = $document->getDocumentType()->getDocumentType() == 'CNPJ' ? $document->getDocument() : '';
            $std->IE = $document->getDocumentType()->getDocumentType() == 'Inscrição Estadual' ? $document->getDocument() : '';
            $std->CPF = $document->getDocumentType()->getDocumentType() == 'CPF' ? $document->getDocument() : '';
        }
        if ($this->_order->getClient()->getPeopleType() == 'J') {
            $std->email = $this->_order->getClient()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
        } else {
            $std->email = $this->_order->getClient()->getEmail()[0]->getEmail();
        }
        $std->indIEDest = NULL;
        $std->ISUF = NULL;
        $std->IM = NULL;
        $std->idEstrangeiro = NULL;
        return $this->_nfe->tagdest($std);
    }

    protected function getTagEmit() {
        $std = new \stdClass();
        $std->xNome = $this->_order->getProvider()->getName();
        $std->xFant = $this->_order->getProvider()->getAlias() ? : NULL;
        foreach ($this->_order->getProvider()->getDocument() AS $document) {
            $std->CNPJ = $document->getDocumentType()->getDocumentType() == 'CNPJ' ? $document->getDocument() : '';
            $std->IE = $document->getDocumentType()->getDocumentType() == 'Inscrição Estadual' ? $document->getDocument() : '';
            $std->CPF = $document->getDocumentType()->getDocumentType() == 'CPF' ? $document->getDocument() : '';
        }
        $std->CRT = 1;
        return $this->_nfe->tagemit($std);
    }

    protected function getTagEnderEmit() {
        $std = new \stdClass();
        $std->xLgr = $this->_order->getProvider()->getAddress()[0]->getStreet()->getStreet();
        $std->nro = $this->_order->getProvider()->getAddress()[0]->getNumber();
        $std->xCpl = $this->_order->getProvider()->getAddress()[0]->getComplement();
        $std->xBairro = $this->_order->getProvider()->getAddress()[0]->getStreet()->getDistrict()->getDistrict();
        $std->cMun = NULL;

        $std->xMun = $this->_order->getProvider()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getCity();
        $std->UF = $this->_order->getProvider()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getUf();
        $std->CEP = $this->_order->getProvider()->getAddress()[0]->getStreet()->getCep()->getCep();

        $std->cPais = NULL;
        $std->xPais = $this->_order->getProvider()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getCountry()->getCountryName();

        if ($this->_order->getClient()->getPeopleType() == 'J') {
            $std->fone = $this->_order->getProvider()->getPeopleCompany()[0]->getEmployee()->getPhone()[0]->getDdd() . $this->_order->getProvider()->getPeopleCompany()[0]->getEmployee()->getPhone()[0]->getPhone();
        } else {
            $std->fone = $this->_order->getProvider()->getPhone()[0]->getDdd() . $this->_order->getProvider()->getPhone()[0]->getPhone();
        }

        return $this->_nfe->tagenderEmit($std);
    }

    protected function getTagEnderDest() {
        $std = new \stdClass();
        $std->xLgr = $this->_order->getClient()->getAddress()[0]->getStreet()->getStreet();
        $std->nro = $this->_order->getClient()->getAddress()[0]->getNumber();
        $std->xCpl = $this->_order->getClient()->getAddress()[0]->getComplement();
        $std->xBairro = $this->_order->getClient()->getAddress()[0]->getStreet()->getDistrict()->getDistrict();
        $std->cMun = NULL;
        $std->xMun = $this->_order->getClient()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getCity();
        $std->UF = $this->_order->getClient()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getUf();
        $std->CEP = $this->_order->getClient()->getAddress()[0]->getStreet()->getCep()->getCep();
        $std->cPais = NULL;
        $std->xPais = $this->_order->getClient()->getAddress()[0]->getStreet()->getDistrict()->getCity()->getState()->getCountry()->getCountryName();
        $std->fone = $this->_order->getClient()->getPeopleCompany()[0]->getEmployee()->getPhone()[0]->getDdd() . $this->_order->getProvider()->getPeopleCompany()[0]->getEmployee()->getPhone()[0]->getPhone();
        return $this->_nfe->tagenderDest($std);
    }

}
