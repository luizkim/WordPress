<?php

namespace Sales\Model;

use Core\Model\DefaultModel;
use Core\Model\ErrorModel;
use Core\Helper\Format;
use Company\Model\CompanyModel;
use Carrier\Model\CarrierModel;
use Core\Helper\IP;
use User\Model\UserModel;
use Core\Helper\Api;
use Core\Helper\Mautic;
use Core\Helper\Mail;
use Core\Helper\Config;

class ShippingModel extends DefaultModel {

    protected $_data;
    protected $_filter;
    protected $_carriers;
    protected $_retrieve_tax;
    protected $_denied;
    protected $_restricted;
    protected $_quote;
    protected $_order;
    protected $_client;
    protected $_distance;
    protected $_address_origin;
    protected $_address_destination;
    protected $_params;
    protected $_package;

    /**
     * @var \Company\Model\CompanyModel $entity   
     */
    protected $_companyModel;

    public function initialize(\Zend\ServiceManager\ServiceManager $serviceLocator) {
        parent::initialize($serviceLocator);
        $this->_companyModel = new CompanyModel();
        $this->_companyModel->initialize($serviceLocator);
    }

    public function persistProductType() {
        $carrierModel = new CarrierModel();
        $carrierModel->initialize($this->serviceLocator);
        foreach ($this->_data['request']['product-type'] AS $material) {
            $carrierModel->addProductMaterial($material);
        }
    }

    /**
     * @return \Core\Entity\Order
     */
    public function getOrderFromQuote($quote_id) {
        $query = $this->_em->getRepository('\Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('(O.client =:client OR O.client IS NULL OR O.provider=:provider)')
                ->andWhere('O.quote =:quote')
                ->setParameters(array(
            'quote' => $this->_em->getRepository('\Core\Entity\Quote')->find($quote_id),
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany()
        ));
        $order = $query->getQuery()->getResult();
        return $order ? $order[0] : $order;
    }

    /**
     * @return \Core\Entity\Quote
     */
    public function getQuote($id) {
        $query = $this->_em->getRepository('\Core\Entity\Quote')
                ->createQueryBuilder('Q')
                ->select()
                ->where('(Q.client =:client OR Q.client IS NULL OR Q.provider=:provider)')
                ->andWhere('Q.id =:id')
                ->setParameters(array(
            'id' => $id,
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'provider' => $this->_companyModel->getLoggedPeopleCompany()
        ));
        $quotes = $query->getQuery()->getResult();

        if ($quotes && !$quotes[0]->getClient()) {
            $this->discoveryClientFromOrder($quotes[0]->getOrder());
        }
        $quotes ? $this->changeOrderQuote($quotes[0]) : null;
        return $quotes;
    }

    public function changeOrderQuote(\Core\Entity\Quote $quote) {
        $this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
                ->set('o.quote', $quote->getId())
                ->set('o.price', $quote->getTotal())
                ->where('o.id = :order')
                ->andWhere('o.client = :client')
                ->setParameter('client', $this->_companyModel->getLoggedPeopleCompany()->getId())
                ->setParameter('order', $quote->getOrder())
                ->getQuery()->execute();
    }

    public function discoveryClientFromOrder(\Core\Entity\Order $order) {

        $this->_em->createQueryBuilder()->update('\Core\Entity\Order', 'o')
                ->set('o.client', $this->_companyModel->getLoggedPeopleCompany()->getId())
                ->where('o.id = :order')
                ->andWhere('o.client IS NULL')
                ->setParameter('order', $order)
                ->getQuery()->execute();

        $this->_em->createQueryBuilder()->update('\Core\Entity\Quote', 'q')
                ->set('q.client', $this->_companyModel->getLoggedPeopleCompany()->getId())
                ->where('q.order = :order')
                ->setParameter('order', $order)
                ->getQuery()->execute();
    }

    public function getCompanyContacts() {
        $query = $this->_em->getRepository('\Core\Entity\People')
                ->createQueryBuilder('P')
                ->select()
                ->innerJoin('\Core\Entity\PeopleEmployee', 'PE', 'WITH', 'PE.employee = P.id')
                ->where('(PE.company = :company)');

        $query->setParameters(array(
            'company' => $this->_companyModel->getLoggedPeopleCompany()
        ));
        return $query->getQuery()->getResult();
    }

    /**
     * @return \Core\Entity\Address
     */
    public function getCompanyOriginAddressByCity($city) {
        $query = $this->_em->getRepository('\Core\Entity\Address')
                ->createQueryBuilder('A')
                ->select()
                ->innerJoin('\Core\Entity\Street', 'S', 'WITH', 'S.id = A.street')
                ->innerJoin('\Core\Entity\District', 'D', 'WITH', 'D.id = S.district')
                ->innerJoin('\Core\Entity\City', 'C', 'WITH', 'C.id = D.city')
                ->where('(A.people = :people)')
                ->andWhere('C.id =:city');

        $query->setParameters(array(
            'city' => $city,
            'people' => $this->_companyModel->getLoggedPeopleCompany()
        ));
        return $query->getQuery()->getResult();
    }

    public function quote($params) {
        $this->_data['request'] = $this->checkQuoteData($params);
        if ($params['email']) {
            $userModel = new UserModel();
            $userModel->initialize($this->serviceLocator);
            $email = $userModel->getEmail($params['email']);
            if ($email) {
                $this->_client = count($email->getPeople()->getPeopleCompany()) > 0 ? $email->getPeople()->getPeopleCompany()[0]->getCompany() : NULL;
            } else {
                $companyModel = new CompanyModel();
                $companyModel->initialize($this->serviceLocator);
                Mautic::addDefaultCompany($companyModel->getDefaultCompany());
                Mautic::addProspect($params['name'], $params['email'], $params['phone'], 'prospect,quote,quote-'.strtolower($params['state-origin']));
                Mautic::persist();
                /*
                  try {
                  $SugarAPI = new \SugarAPI\SDK\SugarAPI('https://crm.freteclick.com.br/service/v4_1/rest.php', array('username' => 'luiz.kim', 'password' => 'lw7q8a9w5@@qw'));
                  $SugarAPI->login();
                  $EP = $SugarAPI->createRecord('Accounts');
                  $data = array(
                  'name' => $params['name'],
                  'email1' => $params['email'],
                  'phone' => $params['phone']
                  );
                  $response = $EP->execute($data)->getResponse();
                  if ($response->getStatus() == '200') {
                  $record = $response->getBody(false);
                  $EP2 = $SugarAPI->getRecord('Accounts', $record->id)->execute(array('fields' => 'name,email1,phone'));
                  $getResponse = $EP2->getResponse();
                  print $EP2->getUrl();
                  print_r($getResponse->getBody());
                  }
                  } catch (\SugarAPI\SDK\Exception\AuthenticationException $ex) {
                  print $ex->getMessage();
                  }
                 */
            }
        }

        if (!ErrorModel::getErrors()) {
            $this->persistProductType();
            $params = $this->getCubage($this->_data['request']);
            $params['order'] = $params['order'] ? : 'deadline';
			
/*			
		
            $percentageTax = $this->getAllRegionTax($params);
            foreach ($percentageTax AS $tax) {
                $this->discoveryQuote($tax);
            }			
*/

            $percentageTax = $this->getRegionTax($params);
            foreach ($percentageTax AS $tax) {
                $this->discoveryQuote($tax);
            }
            $fixedTax = $this->getFixedTax();
            foreach ($fixedTax AS $tax) {
                $this->discoveryQuote($tax);
            }
            $percentageOfFreight = $this->getRegionTax($params, 'order');
            foreach ($percentageOfFreight AS $tax) {
                $this->discoveryQuote($tax);
            }

            $percentageOfInvoice = $this->getRegionTax($params, 'invoice');
            foreach ($percentageOfInvoice AS $tax) {
                $this->discoveryQuote($tax);
            }


            $companyTax = $this->companyTax($params);
            foreach ($companyTax AS $tax) {
                $this->discoveryCompanyTax($tax);
            }
            $this->calculatePlan();
            usort($this->_data['quote'], function($a, $b) use ($params) {
                return $a[$params['order']] > $b[$params['order']];
            });
            return $this->_data;
        }
    }

    public function addQuoteDetails($params) {
        if (!$params['order']) {
            ErrorModel::addError('Quote is required');
            return;
        }
        if (!$params['choose-payer']) {
            ErrorModel::addError('Payer is required');
            return;
        }
        $query = $this->_em->getRepository('Core\Entity\Order')
                ->createQueryBuilder('O')
                ->select()
                ->where('(O.client =:client OR O.provider=:provider)')
                ->andWhere('O.id =:id')
                ->andWhere('O.order_status =:order_status')
                ->setParameters(array(
            'id' => $params['order'],
            'provider' => $this->_companyModel->getLoggedPeopleCompany(),
            'client' => $this->_companyModel->getLoggedPeopleCompany(),
            'order_status' => $this->discoveryOrderStatus('quote', 'closed', 1, 0)
        ));
        $result = $query->getQuery()->getResult();
        $order = $result ? $result[0] : NULL;

        if (!$order) {
            ErrorModel::addError('Dont find a order');
            return;
        }
        if (!$order->getAddressDestination()) {
            ErrorModel::addError('Address destination is required');
            return;
        }

        if (!$order->getAddressOrigin()) {
            ErrorModel::addError('Address origin is required');
            return;
        }
        if (!$order->getClient()) {
            ErrorModel::addError('Client is required');
            return;
        }
        if (!$order->getDeliveryContact()) {
            ErrorModel::addError('Delivery contact is required');
            return;
        }
        if (!$order->getDeliveryPeople()) {
            ErrorModel::addError('Delivery people is required');
            return;
        }
        if (!$order->getPrice() || $order->getPrice() < 10) {
            ErrorModel::addError('Price is required');
            return;
        }
        if (!$order->getProvider()) {
            ErrorModel::addError('Provider is required');
            return;
        }
        if (!$order->getQuote()) {
            ErrorModel::addError('Quote is required');
            return;
        }
        if (!$order->getRetrieveContact()) {
            ErrorModel::addError('Retrieve contact is required');
            return;
        }
        if (!$order->getRetrievePeople()) {
            ErrorModel::addError('Retrieve people is required');
            return;
        }
        if ($params['choose-payer'] != $order->getDeliveryPeople()->getId() && $params['choose-payer'] != $order->getRetrievePeople()->getId()) {
            ErrorModel::addError('Payer is wrong');
            return;
        }
        $order->setStatus($this->discoveryOrderStatus('waiting client invoice tax', 'pending', 1, 1));
        $order->setPayer($this->_em->getRepository('\Core\Entity\People')->find($params['choose-payer']));
        $order->setComments($params['order-comments']);
        $this->persist($order);
        //$this->_em->flush($order);
		
		$state = $order->getAddressOrigin()->getStreet()->getDistrict()->getCity()->getState()->getUf();						
		
		$formID = Mautic::getFormID('invoice-tax-instructions',strtolower($state));
        $params_mautic['mauticform[formId]'] = $formID;
        $params_mautic['mauticform[f_key]'] = Mautic::getOAuth2PublicKey();
        $params_mautic['mauticform[email]'] = $order->getClient()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
        $params_mautic['mauticform[body]'] = Mail::renderMail($this->serviceLocator, 'client-invoice-tax.phtml', array(
                    'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                    'order' => $order
        ));
        $client = new \Core\Helper\Api();
        $client->post(Mautic::getMauticFormAction($formID), array('form_params' => $params_mautic));
        return $order;
    }

    protected function verifyRestriction(\Core\Entity\DeliveryTax $tax, $restriction_type = 'delivery_denied') {
        $query = $this->_em->getRepository('\Core\Entity\DeliveryRestrictionMaterial')->createQueryBuilder('DRM')
                ->select()
                ->innerJoin('\Core\Entity\ProductMaterial', 'PM', 'WITH', 'PM.id = DRM.product_material')
                ->andWhere('(DRM.carrier = :carrier)')
                ->andWhere('(PM.material IN (:product_material))')
                ->andWhere('(DRM.restriction_type = :restriction_type)');
        $query->setParameters(array(
            'carrier' => $tax->getCarrier()->getId(),
            'restriction_type' => $restriction_type,
            'product_material' => $this->_data['request']['product-type']
        ));
        return $query->getQuery()->getResult();
    }

    protected function persisteOrderProductpackage(\Core\Entity\Order $order) {

        foreach ($this->_data['request']['product-package'] AS $package) {
            $orderPackage = new \Core\Entity\OrderPackage();
            $orderPackage->setOrder($order);
            $orderPackage->setDepth($package['depth']);
            $orderPackage->setHeight($package['height']);
            $orderPackage->setQtd($package['qtd']);
            $orderPackage->setWeight($package['weight']);
            $orderPackage->setWidth($package['width']);
            $this->persist($orderPackage);
            //$this->_em->flush($orderPackage);
        }
    }

    public function discoveryInvoiceStatus($status_name, $real_status = 'open', $system = 0, $notify = 0) {
        $orderStatus = $this->_em->getRepository('\Core\Entity\InvoiceStatus')->findOneBy(array('status' => $status_name));
        if (!$orderStatus) {
            $orderStatus = new \Core\Entity\InvoiceStatus();
            $orderStatus->setNotify($notify);
            $orderStatus->setRealStatus($real_status);
            $orderStatus->setStatus($status_name);
            $orderStatus->setSystem($system);
            $this->persist($orderStatus);
            //$this->_em->flush($orderStatus);
        }
        return $orderStatus;
    }

    public function discoveryOrderStatus($status_name, $real_status = 'open', $system = 0, $notify = 0) {
        $orderStatus = $this->_em->getRepository('\Core\Entity\OrderStatus')->findOneBy(array('status' => $status_name));
        if (!$orderStatus) {
            $orderStatus = new \Core\Entity\OrderStatus();
            $orderStatus->setNotify($notify);
            $orderStatus->setRealStatus($real_status);
            $orderStatus->setStatus($status_name);
            $orderStatus->setSystem($system);
            $this->persist($orderStatus);
            //$this->_em->flush($orderStatus);
        }
        return $orderStatus;
    }

    protected function persistQuote(\Core\Entity\DeliveryTax $tax) {	

        $carrier_id = $tax->getCarrier()->getId();
        $companyModel = new CompanyModel();
        $companyModel->initialize($this->serviceLocator);

        $userModel = new UserModel();
        $userModel->initialize($this->serviceLocator);
        $this->_client = $this->_client? : ($userModel->getLoggedUser() ? $this->_companyModel->getLoggedPeopleCompany() : NULL);

        if (!$this->_order) {
            $orderStatus = $this->discoveryOrderStatus('quote', 'closed', 1, 0);
            $this->_order = new \Core\Entity\Order();
            $this->_order->setClient($this->_client);
            $this->_order->setProvider($companyModel->getDefaultCompany());
            $this->_order->setPrice(0);
            $this->_order->setCubage($this->_data['request']['final-weight']);
            $this->_order->setProductType(implode(', ', $this->_data['request']['product-type']));
            $this->_order->setInvoiceTotal($this->_data['request']['product-total-price']);
            $this->_order->setStatus($orderStatus);
            $this->_order->setAddressDestination($this->_address_destination);
            $this->_order->setAddressOrigin($this->_address_origin);

            $this->persist($this->_order);
            //$this->_em->flush($this->_order);
            $this->_data['id'] = $this->_order->getId();
            $this->persisteOrderProductpackage($this->_order);
        }

        if (!$this->_quote[$carrier_id]) {
            $city_origin = $this->_em->getRepository('Core\Entity\City')->find($this->_data['request']['city-origin-id']);
            $city_destination = $this->_em->getRepository('Core\Entity\City')->find($this->_data['request']['city-destination-id']);
            $this->_quote[$carrier_id] = new \Core\Entity\Quote();
            $this->_quote[$carrier_id]->setCityDestination($city_destination);
            $this->_quote[$carrier_id]->setCityOrigin($city_origin);
            $this->_quote[$carrier_id]->setCarrier($tax->getCarrier());
            $this->_quote[$carrier_id]->setProvider($companyModel->getDefaultCompany());
            $this->_quote[$carrier_id]->setIp(IP::getIpAddress());
            $this->_quote[$carrier_id]->setInternalIp(IP::getInternalIpAddress());
            $this->_quote[$carrier_id]->setClient($this->_client);
            $this->_quote[$carrier_id]->setDeadline($tax->getRegionDestination() ? $tax->getRegionDestination()->getDeadline() : 0);
            $this->_quote[$carrier_id]->setDenied($this->_denied[$carrier_id]? : false);
            $this->_quote[$carrier_id]->setTotal($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['total']);
            $this->_quote[$carrier_id]->setOrder($this->_order);
        } else {
            $tax->getRegionDestination() && $tax->getRegionDestination()->getDeadline() > $this->_quote[$carrier_id]->getDeadline() ? $this->_quote[$carrier_id]->setDeadline($tax->getRegionDestination()->getDeadline()) : null;
            $this->_quote[$carrier_id]->setTotal($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['total']);
        }

        $this->persist($this->_quote[$carrier_id]);
        //$this->_em->flush($this->_quote[$carrier_id]);

        $this->persistTax($tax);
        return $this->_quote[$carrier_id];
    }

    protected function persistTax(\Core\Entity\DeliveryTax $tax) {
        $carrier_id = $tax->getCarrier()->getId();
        $quote_details = new \Core\Entity\QuoteDetail();
        $quote_details->setQuote($this->_quote[$carrier_id]);
        $quote_details->setTaxName($tax->getTaxName());
        $quote_details->setTaxType($tax->getTaxType());
        $quote_details->setTaxSubtype($tax->getTaxSubtype());
        $quote_details->setDeliveryTax($tax);
        $quote_details->setMinimumPrice($tax->getMinimumPrice());
        $quote_details->setFinalWeight($this->_data['request']['final-weight']);
        $quote_details->setRegionOrigin($tax->getRegionOrigin());
        $quote_details->setRegionDestination($tax->getRegionDestination());
        $quote_details->setTaxOrder($tax->getTaxOrder());
        $quote_details->setPrice($tax->getPrice());
        $quote_details->setOptional($tax->getOptional());
        $quote_details->setPriceCalculated($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['tax'][$tax->getId()]['subtotal']);
        $this->persist($quote_details);
        //$this->_em->flush($quote_details);
    }

    protected function persistRetrieveTax(\Core\Entity\DeliveryTax $tax) {
        $carrier_id = $tax->getCarrier()->getId();
        $quote_details = new \Core\Entity\QuoteDetail();
        $quote_details->setQuote($this->_quote[$carrier_id]);
        $quote_details->setTaxName('Taxa de coleta');
        $quote_details->setTaxType('fixed');
        $quote_details->setTaxSubtype(NULL);
        $quote_details->setDeliveryTax(NULL);
        $quote_details->setMinimumPrice(0);
        $quote_details->setFinalWeight(0);
        $quote_details->setRegionOrigin($tax->getRegionOrigin());
        $quote_details->setRegionDestination($tax->getRegionDestination());
        $quote_details->setTaxOrder(0);
        $quote_details->setPrice($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['retrieve-tax']);
        $quote_details->setOptional(0);
        $quote_details->setPriceCalculated($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['retrieve-tax']);
        $this->persist($quote_details);
        //$this->_em->flush($quote_details);
    }

    protected function calculatePlan() {
        $tax_percentage = 15;
        $minimum_price = 10;
        foreach ($this->_data['quote'] AS $carrier_id => $c) {
            $price = $this->_data['quote'][$carrier_id]['total'] / 100 * $tax_percentage;
            $subtotal = $price > $minimum_price ? $price : $minimum_price;
            $this->_data['quote'][$carrier_id]['total'] += $subtotal;
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['tax-name'] = 'Taxa de conveniência';
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['tax-type'] = 'percentage';
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['tax-subtype'] = 'order';
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['price'] = $tax_percentage;
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['minimum-price'] = $minimum_price;
            $this->_data['quote'][$carrier_id]['tax']['plan-1']['subtotal'] = $subtotal;
            $this->persistPlan($this->_data['quote'][$carrier_id]['tax']['plan-1'], $carrier_id);
        }
    }

    protected function persistPlan($plan, $carrier_id) {
        $quote_details = new \Core\Entity\QuoteDetail();
        $quote_details->setQuote($this->_quote[$carrier_id]);
        $quote_details->setTaxName($plan['tax-name']);
        $quote_details->setTaxType($plan['tax-type']);
        $quote_details->setTaxSubtype($plan['tax-subtype']);
        $quote_details->setMinimumPrice($plan['minimum-price']);
        $quote_details->setFinalWeight(0);
        $quote_details->setTaxOrder(0);
        $quote_details->setPrice($plan['price']);
        $quote_details->setOptional(0);
        $quote_details->setPriceCalculated($plan['subtotal']);
        $this->persist($quote_details);
        //$this->_em->flush($quote_details);
        $this->_quote[$carrier_id]->setTotal($this->_data['quote'][$carrier_id]['total']);
        $this->persist($this->_quote[$carrier_id]);
        //$this->_em->flush($this->_quote[$carrier_id]);
    }

    protected function persistCompanyTax(\Core\Entity\CompanyTax $tax, $carrier_id) {
        $quote_details = new \Core\Entity\QuoteDetail();
        $quote_details->setQuote($this->_quote[$carrier_id]);
        $quote_details->setTaxName($tax->getTaxName());
        $quote_details->setTaxType($tax->getTaxType());
        $quote_details->setTaxSubtype($tax->getTaxSubtype());
        $quote_details->setMinimumPrice($tax->getMinimumPrice());
        $quote_details->setFinalWeight(0);
        $quote_details->setTaxOrder($tax->getTaxOrder());
        $quote_details->setPrice($tax->getPrice());
        $quote_details->setOptional($tax->getOptional());
        $quote_details->setPriceCalculated($this->_data[$this->_denied[$carrier_id] ? 'quote_denied' : 'quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['subtotal']);
        $this->persist($quote_details);
        //$this->_em->flush($quote_details);
        $this->_quote[$carrier_id]->setTotal($this->_data['quote'][$carrier_id]['total']);
        $this->persist($this->_quote[$carrier_id]);
        //$this->_em->flush($this->_quote[$carrier_id]);
    }

    protected function discoveryCompanyTax(\Core\Entity\CompanyTax $tax) {
        foreach ($this->_data['quote'] AS $carrier_id => $c) {
            $price = $this->_data['quote'][$carrier_id]['total'] / (100 - $tax->getPrice()) * $tax->getPrice();
            $subtotal = $price > $tax->getMinimumPrice() ? $price : $tax->getMinimumPrice();
            $this->_data['quote'][$carrier_id]['total'] += $subtotal;
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['tax-name'] = $tax->getTaxName();
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['tax-type'] = $tax->getTaxType();
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['tax-subtype'] = $tax->getTaxSubtype();
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['price'] = $tax->getPrice();
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['minimum-price'] = $tax->getMinimumPrice();
            $this->_data['quote'][$carrier_id]['tax']['tax-' . $tax->getId()]['subtotal'] = $subtotal;
            $this->persistCompanyTax($tax, $carrier_id);
        }
    }

    protected function discoveryQuote(\Core\Entity\DeliveryTax $tax) {
        $carrier_id = $tax->getCarrier()->getId();
        if (!$this->_denied[$carrier_id] && !$this->_filter[$carrier_id]) {
            $this->_denied[$carrier_id] = $this->verifyRestriction($tax);
            $this->_denied[$carrier_id] ? ($this->_data['quote_denied'][$carrier_id]['delivery-restricted'] = $this->_denied[$carrier_id]) : false;
        }
        if ($this->_denied[$carrier_id]) {
            $quote_type = 'quote_denied';
        } else {
            $quote_type = 'quote';
        }
        if (!$this->_data[$quote_type][$carrier_id]['delivery-restricted'] && !$this->_filter[$carrier_id]) {
            $this->_data[$quote_type][$carrier_id]['delivery-restricted'] = $this->verifyRestriction($tax, 'delivery_restricted') ? true : false;
        }
        if (!$this->_filter[$carrier_id][$tax->getTaxName()]) {
            $this->_filter[$carrier_id][$tax->getTaxName()] = $tax->getId();
            $this->_carriers[$carrier_id] = $carrier_id;
            $this->_data[$quote_type][$carrier_id]['logo'] = $tax->getCarrier()->getImage() ? $tax->getCarrier()->getImage()->getUrl() : NULL;
            $this->_data[$quote_type][$carrier_id]['carrier-name'] = $tax->getCarrier()->getName();
            $this->_data[$quote_type][$carrier_id]['carrier-alias'] = $tax->getCarrier()->getAlias();
            $this->_data[$quote_type][$carrier_id]['carrier-active'] = $tax->getCarrier()->getEnabled()?'carrier-enabled':'carrier-disabled';
            $this->_data[$quote_type][$carrier_id]['carrier-logo'] = $tax->getCarrier()->getImage() ? '//' . $this->_data['request']['domain'] . $tax->getCarrier()->getImage()->getUrl() : null;
            $this->_data[$quote_type][$carrier_id]['deadline'] = $tax->getRegionDestination() ? ($tax->getRegionDestination()->getDeadline() > $this->_data[$quote_type][$carrier_id]['deadline'] ? $tax->getRegionDestination()->getDeadline() : $this->_data[$quote_type][$carrier_id]['deadline']) : ($this->_data[$quote_type][$carrier_id]['deadline'] ? : null);
            $subtotal = $this->quoteCalculateByType($tax);
            if (!$subtotal) {
                //unset($this->_data[$quote_type][$tax->getCarrier()->getId()]);
                return;
            }
            $this->_data[$quote_type][$carrier_id]['total'] += $subtotal;
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['tax-name'] = $tax->getTaxName();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['tax-type'] = $tax->getTaxType();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['tax-subtype'] = $tax->getTaxSubtype();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['price'] = $tax->getPrice();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['minimum-price'] = $tax->getMinimumPrice();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['weight'] = $tax->getFinalWeight();
            $this->_data[$quote_type][$carrier_id]['tax'][$tax->getId()]['subtotal'] = $subtotal;
            $quote = $this->persistQuote($tax);
            $this->_data[$quote_type][$carrier_id]['quote-id'] = $quote->getId();
            $this->_data[$quote_type][$carrier_id]['order-id'] = $quote->getOrder()->getId();
            if (!isset($this->_retrieve_tax[$carrier_id]) && $tax->getRegionOrigin() && $tax->getRegionOrigin()->getRetrieveTax() > 0) {
                $this->_retrieve_tax[$carrier_id] = $tax->getRegionOrigin()->getRetrieveTax();
                $this->_data[$quote_type][$carrier_id]['total'] += $this->_retrieve_tax[$carrier_id];
                $this->_data[$quote_type][$carrier_id]['retrieve-tax'] = $this->_retrieve_tax[$carrier_id];
                $this->persistRetrieveTax($tax);
            }
        }
    }

    /*
      $address = $this->_em->getRepository('\Core\Entity\Address')->find($address->getId());


      $street = $address->getStreet();
      $cep = $street->getCep();
      $text = $cep->getStreet()->getStreet();
      $text .=', ' . $address->getNumber();
      $district = $street->getDistrict();
      $text .= ', ' . $district->getDistrict();
      $city = $district->getCity();
      $text .= ', ' . $city->getCity()->getCity();
      $text .= ', ' . $city->getState()->getUf();
      echo $text;
      die();
      return $text;
     * 
     */

    protected function getAddressDestinationString() {
        return $this->_params['street-destination'] . ', ' . $this->_params['address-number-destination'] . ' - ' . $this->_params['district-destination'] . ' - ' . $this->_params['city-destination'] . ' - ' . $this->_params['state-destination'];
    }

    protected function getAddressOriginString() {
        return $this->_params['street-origin'] . ', ' . $this->_params['address-number-origin'] . ' - ' . $this->_params['district-origin'] . ' - ' . $this->_params['city-origin'] . ' - ' . $this->_params['state-origin'];
    }

    protected function getDistanceFromCarrier(\Core\Entity\People $carrier) {
        if (!$this->_distance[$carrier->getId()]) {
            //$distance_retrieve = \Core\Helper\GMaps::distanceMatrix($this->getAddressString($carrier->getAddress()[0]), $this->getAddressString($this->_address_origin));
            $distance_delivery = \Core\Helper\GMaps::distanceMatrix($this->getAddressOriginString(), $this->getAddressDestinationString());
            $this->_distance[$carrier->getId()] = $distance_delivery / 1000;
        }
        return $this->_distance[$carrier->getId()];
    }

    protected function quoteCalculateByType(\Core\Entity\DeliveryTax $tax, $quote_type = 'quote') {
        if ($tax->getTaxType() == 'fixed' && $tax->getTaxSubtype() != 'kg' && $tax->getTaxSubtype() != 'km') {
            $price = $tax->getPrice();
        }
        if ($tax->getTaxType() == 'fixed' && $tax->getTaxSubtype() == 'km') {
            $price = $this->getDistanceFromCarrier($tax->getCarrier()) * $tax->getPrice();
            if (!$price) {
                unset($this->_data[$quote_type][$tax->getCarrier()->getId()]);
                return;
            }
        }
        if ($tax->getTaxType() == 'fixed' && $tax->getTaxSubtype() == 'kg') {						
			if (!$tax->getRegionDestination()) {
				$price = $this->_data['request']['final-weight'] / $tax->getMinimumPrice();								
				$price = $price > 0 ? ceil($price) * $tax->getPrice(): 0;								
			}else{
				$price = ($this->_data['request']['final-weight'] - $tax->getFinalWeight()) * $tax->getPrice();
			}											
        }
        if ($tax->getTaxType() == 'percentage' && $tax->getTaxSubtype() == 'invoice') {
            $price = $this->_data['request']['product-total-price'] / 100 * $tax->getPrice();
        } elseif ($tax->getTaxType() == 'percentage' && $tax->getTaxSubtype() == 'order') {
            $price = $this->_data[$quote_type][$tax->getCarrier()->getId()]['total'] / 100 * $tax->getPrice();
        }		
		
		$price = is_infinite($price) && is_float($price) ? 1:   $price > 0 ? $price : 0;
		
        if ($tax->getMinimumPrice() < 0) {
            $total = $price > $tax->getMinimumPrice() ? $tax->getMinimumPrice() : $price;
        } else {			
			if (!$tax->getRegionDestination() && $tax->getTaxType() == 'fixed' && $tax->getTaxSubtype() == 'kg') {
				$total = $price;
			}else{						
				$total = $price > $tax->getMinimumPrice() ? $price : $tax->getMinimumPrice();
			}
        }
        return $total;
    }

    protected function checkAdressData($params, $remove) {
        foreach ($params AS $key => $d) {
            $data[str_replace('-' . $remove, '', $key)] = $d;
        }
        return $data;
    }

    protected function addQuoteAddress($params) {
        $params['address-nickname'] = 'Endereço Principal';
        $addressModel = new \Core\Model\AddressModel();
        $addressModel->initialize($this->serviceLocator);
        $data = $this->checkAdressData($params, 'origin');
        $this->_address_origin = $addressModel->addPeopleAddress(NULL, $data);
        $data = $this->checkAdressData($params, 'destination');
        $this->_address_destination = $addressModel->addPeopleAddress(NULL, $data);
        return $this->discoveryCityFromCep($params);
    }

    protected function discoveryCityFromCep($params) {

        $street = $this->_address_origin->getStreet();
        $params['street-origin'] = $street->getStreet();
        $district = $street->getDistrict();
        $params['district-origin'] = $district->getDistrict();
        $city = $district->getCity();
        $params['city-origin-id'] = $city->getId();
        $params['city-origin'] = $city->getCity();
        $state = $city->getState();
        $params['state-origin'] = $state->getUf();

        $street_d = $this->_address_destination->getStreet();
        $params['street-destination'] = $street_d->getStreet();
        $district_d = $street_d->getDistrict();
        $params['district-destination'] = $district_d->getDistrict();
        $city_d = $district_d->getCity();
        $params['city-destination-id'] = $city_d->getId();
        $params['city-destination'] = $city_d->getCity();
        $state_d = $city_d->getState();
        $params['state-destination'] = $state_d->getUf();
        return $params;
    }

    protected function checkQuoteData($params) {
        if (!$params['cep-origin']) {
            ErrorModel::addError('CEP origin is required');
            return;
        }
        if (!$params['cep-destination']) {
            ErrorModel::addError('CEP destination is required');
            return;
        }
        $params = $this->addQuoteAddress($params);
        $this->_params = $params;
        if (!$params['product-type']) {
            ErrorModel::addError('Product type is required');
        } else {
            $params['product-type'] = array_map('trim', array_filter(explode(',', $params['product-type'])));
        }
        if (!$params['city-origin'] && !$params['city-origin-id']) {
            ErrorModel::addError('City origin is required');
        }
        if (!$params['state-origin'] && !$params['city-origin-id']) {
            ErrorModel::addError('State origin is required');
        }
        if (!$params['city-destination'] && !$params['city-destination-id']) {
            ErrorModel::addError('City destination is required');
        }
        if (!$params['state-destination'] && !$params['city-destination-id']) {
            ErrorModel::addError('State destination is required');
        }
        $params['product-total-price'] = Format::realToCurrency($params['product-total-price']);
        if (!$params['product-total-price']) {
            ErrorModel::addError('Product price is required');
        }
        if (!$params['product-package']) {
            ErrorModel::addError('Product package is required');
        }
        foreach ($params['product-package'] AS $key => $package) {

            $params['product-package'][$key]['height'] = Format::realToCurrency($package['height']);
            $params['product-package'][$key]['width'] = Format::realToCurrency($package['width']);
            $params['product-package'][$key]['depth'] = Format::realToCurrency($package['depth']);
            $params['product-package'][$key]['weight'] = Format::realToCurrency($package['weight']);

            $this->_package['height'] = $this->_package['height'] > $params['product-package'][$key]['height'] ? $this->_package['height'] : $params['product-package'][$key]['height'];
            $this->_package['width'] = $this->_package['width'] > $params['product-package'][$key]['width'] ? $this->_package['width'] : $params['product-package'][$key]['width'];
            $this->_package['depth'] = $this->_package['depth'] > $params['product-package'][$key]['depth'] ? $this->_package['depth'] : $params['product-package'][$key]['depth'];

            if ($params['product-package'][$key]['height'] > Config::getConfig('package-max-height')) {
                ErrorModel::addError(array('message' => 'Maximum of package height is %1$s meters', 'values' => array(Config::getConfig('package-max-height'))));
            }
            if ($params['product-package'][$key]['width'] > Config::getConfig('package-max-width')) {
                ErrorModel::addError(array('message' => 'Maximum of package width is %1$s meters', 'values' => array(Config::getConfig('package-max-width'))));
            }
            if ($params['product-package'][$key]['depth'] > Config::getConfig('package-max-depth')) {
                ErrorModel::addError(array('message' => 'Maximum of package depth is %1$s meters', 'values' => array(Config::getConfig('package-max-depth'))));
            }
            if ($params['product-package'][$key]['weight'] > Config::getConfig('package-max-weight')) {
                ErrorModel::addError(array('message' => 'Maximum of package weight is %1$s kg', 'values' => array(Config::getConfig('package-max-weight'))));
            }
            if (!$package['qtd']) {
                ErrorModel::addError('Package qtd is required');
            }
            if (!$params['product-package'][$key]['height']) {
                ErrorModel::addError('Package height is required');
            }
            if (!$params['product-package'][$key]['width']) {
                ErrorModel::addError('Package width is required');
            }
            if (!$params['product-package'][$key]['depth']) {
                ErrorModel::addError('Package depth is required');
            }
            if (!$params['product-package'][$key]['weight']) {
                ErrorModel::addError('Package weight is required');
            }
        }
        return $params;
    }

    /*
      02 caixas de 50 cm x 50 cm x 50 cm
      0,50 x 0,50 x 0,50 =  0,125
      0,125 x 300 = 37,50 KG
      37,5 kg x 02 volumes = 75 KG
     */

    public function getCubage($params) {
        foreach ($params['product-package'] AS $package) {
            $cubage = $cubage + ($package['qtd'] * $package['height'] * $package['width'] * $package['depth'] * 300);
            $weight = $weight + ($package['qtd'] * $package['weight']);            
        }
		$params['weight'] = $weight;
        $params['cubage'] = $cubage;
        $params['final-weight'] = ($cubage > $weight ? $cubage : $weight);		
		
        $this->_data['request'] = $params;
        $this->_data['request']['product-package'] = $params['product-package'];
        $this->_package['cubage'] = $params['final-weight'];

        return $params;
    }

    public function companyTax($params) {
        $query = $this->_em->getRepository('\Core\Entity\CompanyTax')->createQueryBuilder('t')
                ->select()
                ->innerJoin('\Core\Entity\City', 'CITY_ORIG', 'WITH', 'CITY_ORIG.state = t.state_origin')
                ->innerJoin('\Core\Entity\City', 'CITY_DEST', 'WITH', 'CITY_DEST.state = t.state_destination')
                ->innerJoin('\Core\Entity\People', 'COMPANY', 'WITH', 'COMPANY.id = t.people')
                ->where('t.optional = 0')
                ->andWhere('(COMPANY.id = :company)')
                ->andWhere('((CITY_ORIG.city = :city_origin OR CITY_ORIG.id = :city_origin_id) AND (CITY_ORIG.state = :state_origin OR CITY_ORIG.id = :city_origin_id))')
                ->andWhere('((CITY_DEST.city = :city_destination  OR CITY_DEST.id = :city_destination_id) AND (CITY_DEST.state = :state_destination OR CITY_DEST.id = :city_destination_id))')
                ->andWhere('t.tax_type=:percentage AND t.tax_subtype=:order');


        $query->setParameters(array(
            'company' => $this->_companyModel->getCompanyDomain()->getId(),
            'percentage' => 'percentage',
            'order' => 'order',
            'city_destination_id' => $params['city-destination-id'],
            'city_origin_id' => $params['city-origin-id'],
            'city_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
                        'city' => $params['city-origin'],
                        'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin']))
                    ))->getCity(),
            'state_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin'])),
            'city_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
                        'city' => $params['city-destination'],
                        'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination']))
                    ))->getCity(),
            'state_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination']))
        ));

        return $query->getQuery()->getResult();
    }
	
    public function getAllRegionTax($params) {
		
		
        $subquery = $this->_em->createQueryBuilder()
                ->select('PEOPLE.id')
				->from('\Core\Entity\DeliveryTax', 'DT')
				->innerJoin('\Core\Entity\People', 'PEOPLE', 'WITH', 'DT.carrier = PEOPLE.id')
				->innerJoin('\Core\Entity\PeopleCarrier', 'TCARRIER', 'WITH', 'TCARRIER.carrier = DT.carrier')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'TORIG', 'WITH', 'DT.region_origin = TORIG.id')
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'TDEL_CITY_ORIG', 'WITH', 'TDEL_CITY_ORIG.region = TORIG.id')
                ->innerJoin('\Core\Entity\City', 'TCITY_ORIG', 'WITH', 'TCITY_ORIG.id = TDEL_CITY_ORIG.city')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'TDEST', 'WITH', 'DT.region_destination = TDEST.id')
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'TDEL_CITY_DEST', 'WITH', 'TDEL_CITY_DEST.region = TDEST.id')
                ->innerJoin('\Core\Entity\City', 'TCITY_DEST', 'WITH', 'TCITY_DEST.id = TDEL_CITY_DEST.city')
				->where('(TCARRIER.company = :company)')
                ->andWhere('((TCITY_ORIG.city = :city_origin OR TCITY_ORIG.id = :city_origin_id) AND (TCITY_ORIG.state = :state_origin OR TCITY_ORIG.id = :city_origin_id))')
                ->andWhere('((TCITY_DEST.city = :city_destination  OR TCITY_DEST.id = :city_destination_id) AND (TCITY_DEST.state = :state_destination OR TCITY_DEST.id = :city_destination_id))');
				
		if (!$this->_companyModel->getLoggedPeopleCompany() || $this->_companyModel->getLoggedPeopleCompany()->getId() != $this->_companyModel->getDefaultCompany()->getId()) {
            $subquery->andWhere('(PEOPLE.enable = 1)');
        }

		$subquery->addGroupBy('PEOPLE.id');
		
        //echo $subquery->getQuery()->getSQL();
        //die();
        $query = $this->_em->createQueryBuilder()
				->select('t')
				->from('\Core\Entity\DeliveryTax','t')                
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = t.carrier')                		
				
                ->leftJoin('\Core\Entity\DeliveryRegion', 'ORIG', 'WITH', 't.region_origin = ORIG.id')
                ->leftJoin('\Core\Entity\DeliveryRegionCity', 'DEL_CITY_ORIG', 'WITH', 'DEL_CITY_ORIG.region = ORIG.id')
                ->leftJoin('\Core\Entity\City', 'CITY_ORIG', 'WITH', 'CITY_ORIG.id = DEL_CITY_ORIG.city')
                ->leftJoin('\Core\Entity\DeliveryRegion', 'DEST', 'WITH', 't.region_destination = DEST.id')
                ->leftJoin('\Core\Entity\DeliveryRegionCity', 'DEL_CITY_DEST', 'WITH', 'DEL_CITY_DEST.region = DEST.id')
                ->leftJoin('\Core\Entity\City', 'CITY_DEST', 'WITH', 'CITY_DEST.id = DEL_CITY_DEST.city')								
				->where('t.optional = 0')
                ->andWhere('t.region_origin IS NULL OR ((CITY_ORIG.city = :city_origin OR CITY_ORIG.id = :city_origin_id) AND (CITY_ORIG.state = :state_origin OR CITY_ORIG.id = :city_origin_id))')
                ->andWhere('t.region_destination IS NULL OR ((CITY_DEST.city = :city_destination  OR CITY_DEST.id = :city_destination_id) AND (CITY_DEST.state = :state_destination OR CITY_DEST.id = :city_destination_id))')							
				->andWhere($this->_em->createQueryBuilder()->expr()->in('t.carrier', $subquery->getDQL()))
				->andWhere('(CARRIER.max_height >= :max_height AND CARRIER.max_width >= :max_width AND CARRIER.max_depth >= :max_depth AND CARRIER.max_cubage >= :max_cubage)');

		
        $query->andWhere('
		(t.tax_subtype=:order AND (t.final_weight IS NULL OR (t.final_weight >= :final_weight AND t.tax_type=:percentage AND t.tax_subtype IS NULL) OR (t.final_weight < :final_weight AND t.tax_type=:percentage AND t.tax_subtype=:kg)))
			OR
		(t.tax_subtype=:invoice AND (t.final_weight > :final_weight AND t.tax_type=:percentage AND t.tax_subtype!=:kg))
			OR
		(t.tax_subtype!=:order OR t.tax_subtype IS NULL AND (t.final_weight IS NULL OR (t.final_weight >= :final_weight AND t.tax_type=:fixed AND t.tax_subtype IS NULL) OR (t.final_weight < :final_weight AND t.tax_type=:fixed AND t.tax_subtype=:kg)))		
		');            						

        $query->orderBy('t.final_weight', 'DESC')
				->addGroupBy('t.tax_name')
				->addGroupBy('t.carrier')				
				->setParameters(array(
						'max_height' => $this->_package['height'],
						'max_width' => $this->_package['width'],
						'max_depth' => $this->_package['depth'],
						'max_cubage' => $this->_package['cubage'],
						'company' => $this->_companyModel->getCompanyDomain()->getPeople()->getId(),
						'fixed' => 'fixed',
						'percentage' => 'percentage',
						'order' => 'order',
						'invoice' => 'invoice',
						'kg' => 'kg',
						'city_destination_id' => $params['city-destination-id'],
						'city_origin_id' => $params['city-origin-id'],
						'city_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
									'city' => $params['city-origin'],
									'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin']))
								))->getCity(),
						'state_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin'])),
						'city_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
									'city' => $params['city-destination'],
									'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination']))
								))->getCity(),
						'state_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination'])),
						'final_weight' => $params['final-weight']
					));
        //echo $query->getQuery()->getSQL();
        //die();

        return $query->getQuery()->getResult();
    }	
    public function getRegionTax($params, $tax_subtype = false) {

        $query = $this->_em->getRepository('\Core\Entity\DeliveryTax')->createQueryBuilder('t')
                ->select()
                ->innerJoin('\Core\Entity\DeliveryRegion', 'ORIG', 'WITH', 't.region_origin = ORIG.id')
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'DEL_CITY_ORIG', 'WITH', 'DEL_CITY_ORIG.region = ORIG.id')
                ->innerJoin('\Core\Entity\City', 'CITY_ORIG', 'WITH', 'CITY_ORIG.id = DEL_CITY_ORIG.city')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'DEST', 'WITH', 't.region_destination = DEST.id')
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'DEL_CITY_DEST', 'WITH', 'DEL_CITY_DEST.region = DEST.id')
                ->innerJoin('\Core\Entity\City', 'CITY_DEST', 'WITH', 'CITY_DEST.id = DEL_CITY_DEST.city')
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = t.carrier')
                ->innerJoin('\Core\Entity\People', 'PEOPLE', 'WITH', 'CARRIER.carrier = PEOPLE.id')
                ->where('t.optional = 0')
                ->andWhere('(CARRIER.max_height >= :max_height AND CARRIER.max_width >= :max_width AND CARRIER.max_depth >= :max_depth AND CARRIER.max_cubage >= :max_cubage)')
                ->andWhere('(CARRIER.company = :company)')
                ->andWhere('((CITY_ORIG.city = :city_origin OR CITY_ORIG.id = :city_origin_id) AND (CITY_ORIG.state = :state_origin OR CITY_ORIG.id = :city_origin_id))')
                ->andWhere('((CITY_DEST.city = :city_destination  OR CITY_DEST.id = :city_destination_id) AND (CITY_DEST.state = :state_destination OR CITY_DEST.id = :city_destination_id))');
        //->andWhere('t.final_weight IS NULL OR t.final_weight >= :final_weight OR (t.final_weight < :final_weight AND t.tax_type=:fixed AND t.tax_subtype=:kg)');


        if (!$this->_companyModel->getLoggedPeopleCompany() || $this->_companyModel->getLoggedPeopleCompany()->getId() != $this->_companyModel->getDefaultCompany()->getId()) {
            $query->andWhere('(PEOPLE.enable = 1)');
        }
        if ($tax_subtype == 'order') {
            $query->andWhere('(t.final_weight IS NULL OR (t.final_weight >= :final_weight AND t.tax_type=:fixed AND t.tax_subtype IS NULL) OR (t.final_weight < :final_weight AND t.tax_type=:fixed AND t.tax_subtype=:kg))');
            $query->andWhere('t.tax_subtype=:order');
            $fixed = 'percentage';
            $order = 'order';
        } elseif ($tax_subtype == 'invoice') {
            $query->andWhere('(t.final_weight > :final_weight AND t.tax_type=:fixed AND t.tax_subtype!=:kg)');
            $query->andWhere('t.tax_subtype=:order');
            $fixed = 'percentage';
            $order = 'invoice';
        } else {
            $query->andWhere('(t.final_weight IS NULL OR (t.final_weight >= :final_weight AND t.tax_type=:fixed AND t.tax_subtype IS NULL) OR (t.final_weight < :final_weight AND t.tax_type=:fixed AND t.tax_subtype=:kg))');
            $query->andWhere('t.tax_subtype!=:order OR t.tax_subtype IS NULL');
            $fixed = 'fixed';
            $order = 'order';
        }
		
		

        $query->orderBy('t.final_weight', 'ASC')->setParameters(array(
            'max_height' => $this->_package['height'],
            'max_width' => $this->_package['width'],
            'max_depth' => $this->_package['depth'],
            'max_cubage' => $this->_package['cubage'],
            'company' => $this->_companyModel->getCompanyDomain()->getPeople()->getId(),
            'fixed' => $fixed,
            'order' => $order,
            'kg' => 'kg',
            'city_destination_id' => $params['city-destination-id'],
            'city_origin_id' => $params['city-origin-id'],
            'city_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
                        'city' => $params['city-origin'],
                        'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin']))
                    ))->getCity(),
            'state_origin' => $params['city-origin-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-origin'])),
            'city_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\City')->findOneBy(array(
                        'city' => $params['city-destination'],
                        'state' => $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination']))
                    ))->getCity(),
            'state_destination' => $params['city-destination-id'] ? '' : $this->_em->getRepository('\Core\Entity\State')->findOneBy(array('uf' => $params['state-destination'])),
            'final_weight' => $params['final-weight']
        ));


        //echo $query->getQuery()->getSQL();
        //die();

        return $query->getQuery()->getResult();
    }

    public function getFixedTax() {
        $query = $this->_em->getRepository('\Core\Entity\DeliveryTax')->createQueryBuilder('t')
                ->select()
                ->where('t.optional = 0')
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = t.carrier')
                ->innerJoin('\Core\Entity\People', 'PEOPLE', 'WITH', 'CARRIER.carrier = PEOPLE.id')
                ->andWhere('(CARRIER.max_height >= :max_height AND CARRIER.max_width >= :max_width AND CARRIER.max_depth >= :max_depth AND CARRIER.max_cubage >= :max_cubage)')
                ->andWhere('(CARRIER.company = :company)')
                ->andWhere('t.carrier IN (:carrier)')
                ->andWhere('t.region_origin IS NULL')
                ->andWhere('t.region_destination IS NULL');


        if (!$this->_companyModel->getLoggedPeopleCompany() || $this->_companyModel->getLoggedPeopleCompany()->getId() != $this->_companyModel->getDefaultCompany()->getId()) {
            $query->andWhere('(PEOPLE.enable = 1)');
        }

        $query->orderBy('t.final_weight')
                ->setParameters(
                        array(
                            'max_height' => $this->_package['height'],
                            'max_width' => $this->_package['width'],
                            'max_depth' => $this->_package['depth'],
                            'max_cubage' => $this->_package['cubage'],
                            'carrier' => $this->_carriers,
                            'company' => $this->_companyModel->getCompanyDomain()->getId()
        ));

        return $query->getQuery()->getResult();
    }

    public function searchRegionOrigin($params, $limit = 30) {
        $query = $this->_em->getRepository('\Core\Entity\City')->createQueryBuilder('C')
                ->select()
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'DRC', 'WITH', 'DRC.city = C.id')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'DR', 'WITH', 'DRC.region = DR.id')
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = DR.people')
                ->innerJoin('\Core\Entity\DeliveryTax', 'DT', 'WITH', 'DT.region_origin = DR.id')
                ->andWhere('CARRIER.company = :company')
                ->andWhere('C.city LIKE :term')
                ->groupBy('C.id')
                ->orderBy('C.city')
                ->setMaxResults($limit)
                ->setParameters(
                        array(
                            'term' => Format::searchNormalize($params['term']) . '%',
                            'company' => $this->_companyModel->getCompanyDomain()->getId()
                ))
                ->getQuery();
        return $query->getResult();
    }

    public function searchRegionDestination($params, $limit = 30) {
        $query = $this->_em->getRepository('\Core\Entity\City')->createQueryBuilder('C')
                ->select()
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'DRC', 'WITH', 'DRC.city = C.id')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'DR', 'WITH', 'DRC.region = DR.id')
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = DR.people')
                ->innerJoin('\Core\Entity\DeliveryTax', 'DT', 'WITH', 'DT.region_destination = DR.id')
                ->andWhere('CARRIER.company = :company')
                ->andWhere('C.city LIKE :term')
                ->groupBy('C.id')
                ->orderBy('C.city')
                ->setMaxResults($limit)
                ->setParameters(
                        array(
                            'term' => Format::searchNormalize($params['term']) . '%',
                            'company' => $this->_companyModel->getCompanyDomain()->getId()
                ))
                ->getQuery();
        return $query->getResult();
    }

    public function getAllRegionOrigin() {

        $query = $this->_em->getRepository('\Core\Entity\City')->createQueryBuilder('C')
                ->select()
                ->innerJoin('\Core\Entity\DeliveryRegionCity', 'DRC', 'WITH', 'DRC.city = C.id')
                ->innerJoin('\Core\Entity\DeliveryRegion', 'DR', 'WITH', 'DRC.region = DR.id')
                ->innerJoin('\Core\Entity\PeopleCarrier', 'CARRIER', 'WITH', 'CARRIER.carrier = DR.people')
                ->innerJoin('\Core\Entity\DeliveryTax', 'DT', 'WITH', 'DT.region_origin = DR.id')
                ->andWhere('(CARRIER.company = :company)')
                ->groupBy('C.id')
                ->orderBy('C.city')
                ->setParameters(
                        array(
                            'company' => $this->_companyModel->getCompanyDomain()->getId()
                ))
                ->getQuery();
        return $query->getResult();
    }

}
