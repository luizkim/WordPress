<?php

namespace Sales\Controller;

use Sales\Model\ShippingModel;
use Core\Model\ErrorModel;
use Sales\Model\QuoteModel;
use Sales\Model\OrderModel;
use NFePHP\DA\NFe\Danfe;
use Carrier\Model\InvoiceTaxModel;
use NFePHP\DA\CTe\Dacte;
use Core\Model\AddressModel;
use Core\Helper\Format;
use Core\Model\TaxModel;
use Translate\Model\TranslateModel;
use Carrier\Model\CarrierModel;

class DefaultController extends \Core\Controller\CompanyController {

    protected $_shippingModel;

    /*
     * @todo Arrumar essa permissão
     */

    public function checkPermission() {
        
    }

    public function sendRetrieveMailAction() {
        $params = $this->params()->fromPost();
        if ($params && $params['id']) {
            try {
                $orderModel = new OrderModel();
                $orderModel->initialize($this->serviceLocator);
                $order = $orderModel->getSaleOrder($params['id']);
                $params['mauticform[formId]'] = 2;
                $params['mauticform[f_key]'] = \Core\Helper\Mautic::getOAuth2PublicKey();
                $params['mauticform[body]'] = \Core\Helper\Mail::renderMail($this->serviceLocator, 'retrieve-request.phtml', array(
                            'domain' => $this->_companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain(),
                            'order' => $order
                ));
                $params['mauticform[email]'] = $order->getQuote()->getCarrier()->getPeopleCompany()[0]->getEmployee()->getEmail()[0]->getEmail();
                $client = new \Core\Helper\Api();
                $client->post(\Core\Helper\Mautic::getMauticFormAction(2), array('form_params' => $params));
                return true;
            } catch (\Exception $ex) {
                ErrorModel::addError($ex->getMessage());
            }
        } else {
            ErrorModel::addError('Error on send e-mail');
        }
    }

    public function invoiceTaxAction() {
        $orderModel = new OrderModel();
        $orderModel->initialize($this->serviceLocator);
        $invoiceTax = $orderModel->getInvoiceTax($this->params()->fromQuery('id'));
        try {
            if ($invoiceTax) {
                if ($invoiceTax->getOrder()[0]->getInvoiceType() == 55) {
                    $danfe = new Danfe($invoiceTax->getInvoice(), 'P', 'A4', 'images/logo.jpg', 'I', '');
                    $id = $danfe->montaDANFE();
                    $pdf = $danfe->render();
                }
                if ($invoiceTax->getOrder()[0]->getInvoiceType() == 57) {
                    $danfe = new Dacte($invoiceTax->getInvoice(), 'P', 'A4', 'images/logo.jpg', 'I', '');
                    $id = $danfe->montaDACTE();
                    $pdf = $danfe->render();
                }
                header('Content-Type: application/pdf');
                echo $pdf;
                exit;
            } else {
                ErrorModel::addError('Access denied on the invoice tax');
            }
        } catch (InvalidArgumentException $e) {
            ErrorModel::addError($e->getMessage());
        }
    }

    public function addAllCarrierInvoiceTaxAction() {
        try {
            $invoice = file_get_contents($this->params()->fromFiles('danfe')['tmp_name']);
            if ($this->params()->fromFiles('danfe')['type'] != 'text/xml') {
                ErrorModel::addError('The invoice must be sent in XML format');
            } elseif ($invoice) {
                $orderModel = new OrderModel();
                $orderModel->initialize($this->serviceLocator);
                $orderModel->discoveryAAAAAAASOrderFromInvoiceTax($invoice);
            } else {
                ErrorModel::addError('Invalid data');
            }
        } catch (InvalidArgumentException $e) {
            ErrorModel::addError($e->getMessage());
        }
    }

    public function addCarrierInvoiceTaxAction() {
        try {
            $params = $this->params()->fromPost();
            $invoice = file_get_contents($this->params()->fromFiles('danfe')['tmp_name']);
            if ($this->params()->fromFiles('danfe')['type'] != 'text/xml') {
                ErrorModel::addError('The invoice must be sent in XML format');
            } elseif ($params && $params['order'] && $invoice) {
                $orderModel = new OrderModel();
                $orderModel->initialize($this->serviceLocator);
                $order = $orderModel->getSaleOrder($params['order']);
                $orderModel->addCarrierInvoiceTax($order, $invoice);
            } else {
                ErrorModel::addError('Invalid data');
            }
        } catch (InvalidArgumentException $e) {
            ErrorModel::addError($e->getMessage());
        }
    }

    public function addClientInvoiceTaxAction() {
        try {
            $params = $this->params()->fromPost();
            $invoice = file_get_contents($this->params()->fromFiles('danfe')['tmp_name']);
            if ($this->params()->fromFiles('danfe')['type'] != 'text/xml') {
                ErrorModel::addError('The invoice must be sent in XML format');
            } elseif ($params && $params['order'] && $invoice) {
                $orderModel = new OrderModel();
                $orderModel->initialize($this->serviceLocator);
                $order = $orderModel->getSaleOrder($params['order']);
                $orderModel->addClientInvoiceTax($order, $invoice);
            } else {
                ErrorModel::addError('Invalid data');
            }
        } catch (InvalidArgumentException $e) {
            ErrorModel::addError($e->getMessage());
        }
    }

    public function orderAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $order_id = $this->params()->fromQuery('id');
        $orderModel = new OrderModel();
        $orderModel->initialize($this->serviceLocator);
        $this->_view->orderStatus = $orderModel->getAllOrderStatus();
        if ($order_id) {
            $order = $orderModel->getPurchasingOrder($order_id);
            $this->_view->order = $order;
            $this->_view->quotes = $orderModel->getAllQuotesFromOrder($order_id);
            $this->_shippingModel = new ShippingModel();
            $this->_shippingModel->initialize($this->serviceLocator);
            $this->_view->quote = $this->_view->quotes ? $this->_view->quotes[0] : $order->getQuote();
            $this->_view->companyAddress = $this->_shippingModel->getCompanyOriginAddressByCity($this->_view->quote->getCityOrigin());
            $this->_view->companyContacts = $this->_shippingModel->getCompanyContacts();
            $this->_view->quoteCompanyOriginContact = $order->getRetrieveContact() ? : ($this->_view->companyContacts ? $this->_view->companyContacts[0] : NULL);
            $this->_view->quoteCompanyDestinationContact = $order->getDeliveryContact() ? : ($this->_view->companyContacts ? $this->_view->companyContacts[0] : NULL);
            $this->_view->quoteCompanyOriginPeople = $order->getRetrievePeople();
            $this->_view->quoteCompanyDestinationPeople = $order->getDeliveryPeople();
            $this->_view->quoteCompanyOriginAddress = $order->getAddressOrigin() ? : ($this->_view->companyAddress ? $this->_view->companyAddress[0] : NULL);
            $this->_view->quoteCompanyDestinationAddress = $order->getAddressDestination() ? : ($this->_view->companyAddress ? $this->_view->companyAddress[0] : NULL);
            $this->_view->clientAddress = NULL;
            $this->_view->clientContacts = NULL;
            $this->_view->quoteClientOriginPeople = $order->getRetrievePeople();
            $this->_view->quoteClientDestinationPeople = $order->getDeliveryPeople();
            $this->_view->quoteClientOriginContact = $order->getRetrieveContact() ? : ( $this->_view->clientContacts ? $this->_view->clientContacts[0] : NULL);
            $this->_view->quoteClientDestinationContact = $order->getDeliveryContact() ? : ($this->_view->clientContacts ? $this->_view->clientContacts[0] : NULL);
            $this->_view->quoteClientOriginAddress = $order->getAddressOrigin() ? : ($this->_view->clientAddress ? $this->_view->clientAddress[0] : NULL);
            $this->_view->quoteClientDestinationAddress = $order->getAddressDestination() ? : ($this->_view->clientAddress ? $this->_view->clientAddress[0] : NULL);
            $this->_view->order = $orderModel->getSaleOrder($order_id);
            $this->_view->client_invoice_tax = $orderModel->getClientInvoiceTaxFromOrder($this->_view->order);
            $this->_view->carrier_invoice_tax = $orderModel->getCarrierInvoiceTaxFromOrder($this->_view->order);



            if ($this->_view->client_invoice_tax && $this->_view->client_invoice_tax->getInvoice()) {
                $nf = simplexml_load_string($this->_view->client_invoice_tax->getInvoice());
                $this->_view->invoice_tax_number = $orderModel->discoveryInvoiceNumber($this->_view->client_invoice_tax);
                $this->_view->invoice_tax_price = $nf->NFe->infNFe->total->ICMSTot->vNF;
            }
            if ($this->_view->carrier_invoice_tax && $this->_view->carrier_invoice_tax->getInvoice()) {
                $this->_view->carrier_invoice_tax_number = $orderModel->discoveryInvoiceNumber($this->_view->carrier_invoice_tax);
                $orderModel->discoveryOrderFromInvoiceTax($this->_view->carrier_invoice_tax);
            }
            if ((count($this->_view->order->getInvoice()) > 0 && $this->_view->order->getStatus()->getStatus() == 'waiting payment') || ($order && count($order->getInvoice()) > 0 && in_array($order->getInvoice()[0]->getInvoice()->getStatus()->getStatus(), array('waiting payment', 'outdated billing', 'open')))) {
                $this->_view->itauData = $orderModel->getItauDataFromOrder($order);
                $this->_view->itauReturn = $orderModel->verifyPayment($order);
            }

            if (count($this->_view->order->getInvoice()) == 0 && in_array($this->_view->order->getStatus()->getStatus(), array('waiting retrieve', 'on the way', 'delivered'))) {
                $orderModel->createInvoiceFromOrder($order);
            }

            if (count($this->_view->order->getInvoice()) > 0) {
                $this->_view->itauStatusData = $orderModel->getItauStatusDataFromOrder($this->_view->order);
            }
            $this->_view->setTemplate('sales/default/order.phtml');
        } else {
            $this->_view->selectedStatus = $this->params()->fromQuery('order-status');
            $this->_view->saleOrders = $orderModel->getSaleOrders($this->params()->fromQuery());
            $this->_view->setTemplate('sales/default/order-list.phtml');
        }


        /*
          $companyModel = new \Company\Model\CompanyModel();
          $companyModel->initialize($this->serviceLocator);
          $domain = $companyModel->getDefaultCompany()->getPeopleDomain()[0]->getDomain();


          $financeModel = new \Finance\Model\FinanceModel();
          $financeModel->initialize($this->serviceLocator);
          echo \Core\Helper\Mail::renderMail($this->serviceLocator, 'client-invoice.phtml', array(
          'saleOrders' => $financeModel->getSalesOrdersFromSaleInvoice($this->_view->order->getInvoice()[0]->getInvoice()->getId()),
          'domain' => $domain, 'order' => $this->_view->order
          ));

          exit(0);
         */
        return $this->_view;
    }

    public function cancelSaleOrderAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $order = $this->params()->fromPost('id');
        $orderModel = new OrderModel();
        $orderModel->initialize($this->serviceLocator);
        $orderModel->cancelSaleOrder($order);
        return $this->_view;
    }

    public function mailInvoiceAction() {
        $order = $this->params()->fromQuery('id');

        $orderModel = new OrderModel();
        $orderModel->initialize($this->serviceLocator);
        $this->_view->order = $orderModel->getEmailSaleOrder($order);
        $this->_view->forceNotLoggedInLayout = true;

        if ((count($this->_view->order->getInvoice()) > 0 && $this->_view->order->getStatus()->getStatus() == 'waiting payment') || ( $this->_view->order && count($this->_view->order->getInvoice()) > 0 && $this->_view->order->getInvoice()[0]->getInvoice()->getStatus()->getStatus() == 'waiting payment')) {
            $this->_view->itauData = $orderModel->getItauDataFromOrder($this->_view->order);
            $this->_view->itauReturn = $orderModel->verifyPayment($this->_view->order);
        }

        if (count($this->_view->order->getInvoice()) > 0) {
            $this->_view->itauStatusData = $orderModel->getItauStatusDataFromOrder($this->_view->order);
        }
        return $this->_view;
    }

    public function addQuoteDetailsAction() {
        $params = $this->params()->fromPost();

        if ($params) {
            $this->_shippingModel = new ShippingModel();
            $this->_shippingModel->initialize($this->serviceLocator);
            $this->_shippingModel->addQuoteDetails($params);
        }
        return $this->_view;
    }

    public function approveOrderAction() {
        $params = $this->params()->fromPost();

        if ($params && $params['id']) {
            $orderModel = new OrderModel();
            $orderModel->initialize($this->serviceLocator);
            $orderModel->approveOrder($params['id']);
        } else {
            ErrorModel::addError('Error on approve this order');
        }
        return $this->_view;
    }

    public function quoteDetailsAction() {
        $params = $this->params()->fromQuery();
        $this->_shippingModel = new ShippingModel();
        $this->_shippingModel->initialize($this->serviceLocator);
        $quote = $this->_shippingModel->getQuote($params['quote']);
        if ($quote) {
            $this->_view->quote = $quote[0];
            $order = $this->_view->quote->getOrder();
            $this->_view->order = $order;
        }
        return $this->_view;
    }

    public function invoiceAction() {
        $order_id = $this->params()->fromQuery('id');
        $invoiceTaxModel = new InvoiceTaxModel();
        $invoiceTaxModel->initialize($this->serviceLocator);
        $invoiceTaxModel->createInvoiceTaxFromOrder($order_id);
        exit(0);
    }

    public function shippingQuoteAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $this->layout()->forceSimpleLayout = true;
        $params = $this->params()->fromPost();
        $this->_shippingModel = new ShippingModel();
        $this->_shippingModel->initialize($this->serviceLocator);
        $params['domain'] = $this->getRequest()->getServer()->get('HTTP_HOST');
        if ($params && $params['product-package']) {
            $tax = $this->_shippingModel->quote($params);
            $this->_view->data = $tax;
            $tax ? false : $this->_view->error = ErrorModel::addError('Don\'t find any quote for this city or product type');
        } else {
            $this->_view->allRegionOrigin = $this->_shippingModel->getAllRegionOrigin();
        }
        $this->_view->error = ErrorModel::getErrors();
        return $this->_view;
    }

    public function chooseQuoteAction() {
        $params = $this->params()->fromQuery();
        if ($params && $params['quote']) {
            $this->_shippingModel = new ShippingModel();
            $this->_shippingModel->initialize($this->serviceLocator);
            $quote = $this->_shippingModel->getQuote($params['quote']);
            if ($quote) {
                $this->_view->quote = $quote[0];
                $order = $this->_view->quote->getOrder();
                $this->_view->order = $order;
                $this->_view->companyAddress = $this->_shippingModel->getCompanyOriginAddressByCity($this->_view->quote->getCityOrigin());
                $this->_view->companyContacts = $this->_shippingModel->getCompanyContacts();
                $this->_view->quoteCompanyOriginContact = $this->_view->order->getRetrieveContact() ? : ($this->_view->companyContacts ? $this->_view->companyContacts[0] : NULL);
                $this->_view->quoteCompanyDestinationContact = $this->_view->order->getDeliveryContact() ? : ($this->_view->companyContacts ? $this->_view->companyContacts[0] : NULL);
                $this->_view->quoteCompanyOriginPeople = $this->_view->order->getRetrievePeople();
                $this->_view->quoteCompanyDestinationPeople = $this->_view->order->getDeliveryPeople();
                $this->_view->quoteCompanyOriginAddress = $this->_view->order->getAddressOrigin() ? : ($this->_view->companyAddress ? $this->_view->companyAddress[0] : NULL);
                $this->_view->quoteCompanyDestinationAddress = $this->_view->order->getAddressDestination() ? : ($this->_view->companyAddress ? $this->_view->companyAddress[0] : NULL);
                $this->_view->clientAddress = NULL;
                $this->_view->clientContacts = NULL;
                $this->_view->quoteClientOriginPeople = $this->_view->order->getRetrievePeople();
                $this->_view->quoteClientDestinationPeople = $this->_view->order->getDeliveryPeople();
                $this->_view->quoteClientOriginContact = $this->_view->order->getRetrieveContact() ? : ( $this->_view->clientContacts ? $this->_view->clientContacts[0] : NULL);
                $this->_view->quoteClientDestinationContact = $this->_view->order->getDeliveryContact() ? : ($this->_view->clientContacts ? $this->_view->clientContacts[0] : NULL);
                $this->_view->quoteClientOriginAddress = $this->_view->order->getAddressOrigin() ? : ($this->_view->clientAddress ? $this->_view->clientAddress[0] : NULL);
                $this->_view->quoteClientDestinationAddress = $this->_view->order->getAddressDestination() ? : ($this->_view->clientAddress ? $this->_view->clientAddress[0] : NULL);
            } else {
                ErrorModel::addError('Quote not found');
            }
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addQuoteOriginCompanyAction() {
        $quoteModel = new QuoteModel();
        $quoteModel->initialize($this->serviceLocator);
        $quoteModel->addQuoteOriginCompany($this->params()->fromPost());
    }

    public function addQuoteOriginClientAction() {
        $quoteModel = new QuoteModel();
        $quoteModel->initialize($this->serviceLocator);
        $quoteModel->addQuoteOriginClient($this->params()->fromPost());
    }

    public function addQuoteDestinationCompanyAction() {
        $quoteModel = new QuoteModel();
        $quoteModel->initialize($this->serviceLocator);
        $quoteModel->addQuoteDestinationCompany($this->params()->fromPost());
    }

    public function addQuoteDestinationClientAction() {
        $quoteModel = new QuoteModel();
        $quoteModel->initialize($this->serviceLocator);
        $quoteModel->addQuoteDestinationClient($this->params()->fromPost());
    }

    public function tableWeightTaxByRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $id = $this->params()->fromPost('company');
        if ($id) {
            $search = $this->params()->fromPost();
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->weightTaxByRegion = $carrierModel->getWeightTaxByRegion($id, $search);
        }
        $this->_view->setTerminal(true);
        $this->_view->setTemplate('carrier/default/tables/weight-tax-by-region.phtml');
        return $this->_view;
    }

    public function searchCityFromCepAction() {
        return array(
            'id' => 3,
            'city' => 'São Paulo',
            'state' => 'SP',
            'district' => 'Jd Aliança'
        );
    }

    public function searchCityOriginAction() {
        $params = $this->params()->fromQuery();
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $result = $shippingModel->searchRegionOrigin($params);
        if ($result) {
            foreach ($result AS $data) {
                $return[$data->getId()]['id'] = $data->getId();
                $return[$data->getId()]['label'] = ucfirst($data->getCity() . ' - ' . $data->getState()->getUf());
                $return[$data->getId()]['value'] = $return[$data->getId()]['label'];
            }
            $this->_view->setVariables(Format::returnData(array('data' => $return)));
        } else {
            ErrorModel::addError(array('code' => '404', 'message' => 'City %1$s not found', 'values' => array($params['term'])));
            $this->_view->setVariables(Format::returnData(ErrorModel::getErrors()));
        }
        return $this->_view;
    }

    public function deleteRestrictionMaterialAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $carrierModel = new CarrierModel();
        $carrierModel->initialize($this->serviceLocator);
        $carrierModel->deleteRestrictionMaterial($this->params()->fromPost('id'));
        return $this->_view;
    }

    public function addRestrictionMaterialAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();

        if ($params && $this->_userModel->loggedIn()) {

            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $translateModel = new TranslateModel();
            $translateModel->initialize($this->serviceLocator);
            $translateModel->setEntity('Core\Entity\Translate');

            $new_restriction = $carrierModel->addRestrictionMaterial($params);
            foreach ($new_restriction AS $key => $data) {
                $translate = $translateModel->translateFromKey($data->getRestrictionType());
                $return[$key]['id'] = $data->getId();
                $return[$key]['product-material'] = $data->getProductMaterial()->getMaterial();
                $return[$key]['restriction-type'] = is_object($translate) ? $translate->getTranslate() : $translate;
            }
            $this->_view->setVariables(Format::returnData($return));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function searchProductTypeAction() {
        $params = $this->params()->fromQuery();
        $carrierModel = new CarrierModel();
        $carrierModel->initialize($this->serviceLocator);
        $result = $carrierModel->searchRestrictionMaterial($params);

        if ($result) {
            foreach ($result AS $data) {
                $return[$data->getId()]['id'] = $data->getId();
                $return[$data->getId()]['label'] = ucfirst($data->getMaterial());
                $return[$data->getId()]['value'] = $return[$data->getId()]['label'];
            }
            $this->_view->setVariables(Format::returnData(array('data' => $return)));
        }
        $this->_view->setVariables(Format::returnData(array('data' => $return)));
        return $this->_view;
    }

    public function searchCityDestinationAction() {
        $params = $this->params()->fromQuery();
        $shippingModel = new ShippingModel();
        $shippingModel->initialize($this->serviceLocator);
        $result = $shippingModel->searchRegionDestination($params);
        if ($result) {
            foreach ($result AS $data) {
                $return[$data->getId()]['id'] = $data->getId();
                $return[$data->getId()]['label'] = ucfirst($data->getCity() . ' - ' . $data->getState()->getUf());
                $return[$data->getId()]['value'] = $return[$data->getId()]['label'];
            }
            $this->_view->setVariables(Format::returnData(array('data' => $return)));
        } else {
            ErrorModel::addError(array('code' => '404', 'message' => 'City %1$s not found', 'values' => array($params['term'])));
            $this->_view->setVariables(Format::returnData(ErrorModel::getErrors()));
        }

        return $this->_view;
    }

    public function tablePercentageTaxByRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $id = $this->params()->fromPost('company');
        if ($id) {
            $search = $this->params()->fromPost();
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->percentageTaxByRegion = $carrierModel->getPercentageTaxByRegion($id, $search);
        }
        $this->_view->setTerminal(true);
        $this->_view->setTemplate('carrier/default/tables/percentage-tax-by-region.phtml');
        return $this->_view;
    }

    public function tableTaxByRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $id = $this->params()->fromPost('company');
        if ($id) {
            $search = $this->params()->fromPost();
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->fixedTaxByRegion = $carrierModel->getFixedTaxByRegion($id, $search);
        }
        $this->_view->setTerminal(true);
        $this->_view->setTemplate('carrier/default/tables/fixed-tax-by-region.phtml');
        return $this->_view;
    }

    public function addAreaCityAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }

        $addressModel = new AddressModel();
        $addressModel->initialize($this->serviceLocator);
        $country = $addressModel->getDefaultCountry();

        $this->_view->states = $addressModel->getStatesByCountry($country);
        $this->_view->cities = $addressModel->getCitiesByState($this->_view->states[0]);

        $params = $this->params()->fromPost();
        if ($params && $this->_userModel->loggedIn()) {
//            $new_address = $this->_companyModel->addCompanyAddress($params);
//            $this->_view->setVariables(Format::returnData(array(
//                        'id' => $new_address->getId(),
//                        'street' => $new_address->getStreet()->getStreet(),
//                        'number' => $new_address->getNumber(),
//                        'complement' => $new_address->getComplement(),
//                        'nickname' => $new_address->getNickname(),
//                        'district' => $new_address->getStreet()->getDistrict()->getDistrict(),
//                        'cep' => $new_address->getStreet()->getCep()->getCep(),
//                        'city' => $new_address->getStreet()->getDistrict()->getCity()->getCity(),
//                        'state' => $new_address->getStreet()->getDistrict()->getCity()->getState()->getState(),
//                        'country' => $new_address->getStreet()->getDistrict()->getCity()->getState()->getCountry()->getCountryname()
//            )));
        }

        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addTaxByWeightAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);

            $new_tax = $taxModel->addTaxByWeight($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'final-weight' => $new_tax->getFinalWeight(),
                        'tax-name' => $new_tax->getTaxName(),
                        'price' => $new_tax->getPrice(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addPercentageTaxAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);

            $new_tax = $taxModel->addPercentageTax($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'tax-name' => $new_tax->getTaxName(),
                        'price' => $new_tax->getPrice(),
                        'tax-subtype' => $new_tax->getTaxSubtype(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function copyPercentageTaxPerRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        $company_id = $this->params()->fromQuery('company_id');
        if ($company_id) {
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->delivery_region = $carrierModel->getRegion($company_id);
            $this->_view->percentageTaxByRegion = $carrierModel->getFixedTaxByRegion($company_id, NULL, 1);
        }

        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);
            $new_tax = $taxModel->copyPercentageTaxByRegion($params);
            $this->_view->setVariables(Format::returnData(array($new_tax ? true : false)));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addPercentageTaxPerRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        $company_id = $this->params()->fromQuery('company_id');
        if ($company_id) {
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->delivery_region = $carrierModel->getRegion($company_id);
            $this->_view->percentageTaxByRegion = $carrierModel->getFixedTaxByRegion($company_id, NULL, 1);
        }

        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);
            $new_tax = $taxModel->addPercentageTaxByRegion($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'tax-name' => $new_tax->getTaxName(),
                        'region_origin' => $new_tax->getRegionOrigin()->getRegion(),
                        'region_destination' => $new_tax->getRegionDestination()->getRegion(),
                        'price' => $new_tax->getPrice(),
                        'tax-subtype' => $new_tax->getTaxSubtype(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional(),
                        'deadline' => $new_tax->getRegionOrigin()->getDeadline(),
                        'final-weight' => $new_tax->getFinalWeight()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addWeightTaxPerRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        $company_id = $this->params()->fromQuery('company_id');
        if ($company_id) {
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->delivery_region = $carrierModel->getRegion($company_id);
            $this->_view->weightTaxByRegion = $carrierModel->getWeightTaxByRegion($company_id, NULL, 1);
        }

        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);
            $new_tax = $taxModel->addWeightTaxByRegion($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'tax-name' => $new_tax->getTaxName(),
                        'region_origin' => $new_tax->getRegionOrigin()->getRegion(),
                        'region_destination' => $new_tax->getRegionDestination()->getRegion(),
                        'price' => $new_tax->getPrice(),
                        'tax-subtype' => $new_tax->getTaxSubtype(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional(),
                        'deadline' => $new_tax->getRegionOrigin()->getDeadline(),
                        'final-weight' => $new_tax->getFinalWeight()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function copyTaxPerRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        $company_id = $this->params()->fromQuery('company_id');
        if ($company_id) {
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->delivery_region = $carrierModel->getRegion($company_id);
            $this->_view->taxByRegion = $carrierModel->getFixedTaxByRegion($company_id, NULL, 1);
        }

        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);
            $new_tax = $taxModel->copyTaxByRegion($params);
            $this->_view->setVariables(Format::returnData(array($new_tax ? true : false)));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addTaxPerRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        $company_id = $this->params()->fromQuery('company_id');
        if ($company_id) {
            $carrierModel = new CarrierModel();
            $carrierModel->initialize($this->serviceLocator);
            $this->_view->delivery_region = $carrierModel->getRegion($company_id);
            $this->_view->fixedTaxByRegion = $carrierModel->getFixedTaxByRegion($company_id, NULL, 1);
        }

        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);
            $new_tax = $taxModel->addTaxByRegion($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'tax-name' => $new_tax->getTaxName(),
                        'region_origin' => $new_tax->getRegionOrigin()->getRegion(),
                        'region_destination' => $new_tax->getRegionDestination()->getRegion(),
                        'price' => $new_tax->getPrice(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional(),
                        'deadline' => $new_tax->getRegionOrigin()->getDeadline(),
                        'final-weight' => $new_tax->getFinalWeight()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addFixedTaxAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();
        if ($params && $this->_userModel->loggedIn()) {
            $taxModel = new TaxModel();
            $taxModel->initialize($this->serviceLocator);

            $new_tax = $taxModel->addFixedTax($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_tax->getId(),
                        'tax-name' => $new_tax->getTaxName(),
                        'price' => $new_tax->getPrice(),
                        'minimum-price' => $new_tax->getMinimumPrice(),
                        'optional' => $new_tax->getOptional()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function addAreaAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $params = $this->params()->fromPost();

        $addressModel = new AddressModel();
        $addressModel->initialize($this->serviceLocator);
        $country = $addressModel->getDefaultCountry();
        $this->_view->states = $addressModel->getStatesByCountry($country);

        if ($params && $this->_userModel->loggedIn()) {
            $new_area = $addressModel->addArea($params);
            $this->_view->setVariables(Format::returnData(array(
                        'id' => $new_area->getId(),
                        'area' => $new_area->getRegion(),
                        'cities' => $params['cities'],
                        'deadline' => $new_area->getDeadline(),
                        'retrieve_tax' => $new_area->getRetrieveTax()
            )));
        }
        $this->_view->setTerminal(true);
        return $this->_view;
    }

    public function deleteTaxAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $taxModel = new TaxModel();
        $taxModel->initialize($this->serviceLocator);
        $taxModel->deleteTax($this->params()->fromPost('id'));
        return $this->_view;
    }

    public function deleteRegionAction() {
        if (ErrorModel::getErrors()) {
            return $this->_view;
        }
        $taxModel = new TaxModel();
        $taxModel->initialize($this->serviceLocator);
        $taxModel->deleteRegion($this->params()->fromPost('id'));
        return $this->_view;
    }

}
